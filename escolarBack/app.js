const express = require('express'),
    webconfig = require('./webconfig'),
    bodyParser = require('body-parser'),
    moment = require('moment');

app = express();

(async () => {
    try {
        app.use(bodyParser.json({ limit: '500mb' }));
        app.use(bodyParser.urlencoded({ limit: '500mb', extended: true }));

        require('./src/api/routes/ping')(app);
        require('./src/api/routes/routes')(app);
        require('./src/api/sync')(app);

        moment.locale('pt-BR');

        app.listen(webconfig.portApi, () => {
            console.log(`[${webconfig.nameApi}] - Ativo | ${webconfig.urlApi}:${webconfig.portApi}`);
        });

    } catch (error) {
        console.log(error.message);
    }
})();