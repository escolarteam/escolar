const fs = require("fs"),
  syncOptions = { alter: true, logging: false };

module.exports = app => {
  fs.readdir("src/core/", (err, files) => {
    /*files.forEach(element => {
      require(`../core/${element}/${element}Model`)().sync(syncOptions);
    });*/
    require(`../core/pessoa/pessoaModel`)().sync(syncOptions);
    require(`../core/planoAula/planoAulaModel`)().sync(syncOptions);
    require(`../core/endereco/enderecoModel`)().sync(syncOptions);
    require(`../core/turma/turmaModel`)().sync(syncOptions);
    require(`../core/rede/redeModel`)().sync(syncOptions);
    require(`../core/perfil/perfilModel`)().sync(syncOptions);
    require(`../core/prato/pratoModel`)().sync(syncOptions);
    require(`../core/tipoAtividade/tipoAtividadeModel`)().sync(syncOptions);
    require(`../core/tipoOcorrencia/tipoOcorrenciaModel`)().sync(syncOptions);
    require(`../core/tipoTelefone/tipoTelefoneModel`)().sync(syncOptions);
    require(`../core/tipoRefeicao/tipoRefeicaoModel`)().sync(syncOptions);
    require(`../core/tipoEmail/tipoEmailModel`)().sync(syncOptions);
    require(`../core/usuario/usuarioModel`)().sync(syncOptions);
    require(`../core/atividade/atividadeModel`)().sync(syncOptions);
    require(`../core/avaliacaoPlano/avaliacaoPlanoModel`)().sync(syncOptions);
    require(`../core/cardapio/cardapioModel`)().sync(syncOptions);
    require(`../core/cardapioRefeicao/cardapioRefeicaoModel`)().sync(syncOptions);
    require(`../core/disciplina/disciplinaModel`)().sync(syncOptions);
    require(`../core/emailPessoa/emailPessoaModel`)().sync(syncOptions);
    require(`../core/matricula/matriculaModel`)().sync(syncOptions);
    require(`../core/ocorrencia/ocorrenciaModel`)().sync(syncOptions);
    require(`../core/ocorrenciaAluno/ocorrenciaAlunoModel`)().sync(syncOptions);
    require(`../core/perfilUsuario/perfilUsuarioModel`)().sync(syncOptions);
    require(`../core/pessoaEndereco/pessoaEnderecoModel`)().sync(syncOptions);
    require(`../core/pessoaFisica/pessoaFisicaModel`)().sync(syncOptions);
    require(`../core/pessoaJuridica/pessoaJuridicaModel`)().sync(syncOptions);
    require(`../core/professorDisciplina/professorDisciplinaModel`)().sync(syncOptions);
    require(`../core/refeicao/refeicaoModel`)().sync(syncOptions);
    require(`../core/refeicaoPrato/refeicaoPratoModel`)().sync(syncOptions);
    require(`../core/telefone/telefoneModel`)().sync(syncOptions);
    
  });
};
