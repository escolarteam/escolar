const webconfig = require("../../webconfig");

module.exports = (res, obj) => {
  if (obj.errorCode) {
    return res.status(obj.errorCode).send(getError(obj, obj.errorCode));
  }
  if (obj.records) {
    obj = obj.records;
  }
  return res.status(200).send(getContent(obj));
};

function getList(obj) {
  if (obj.length) return obj;

  let list = [];
  list.push(obj);
  return list;
}

function getContent(obj) {
  let meta = {};
  meta.server = webconfig.nameApi;
  meta.limit = obj.pg ? obj.pg.limit || 100 : 100;
  meta.offset = obj.pg ? obj.pg.offset || 0 : 0;
  meta.page = obj.pg ? obj.pg.page || 1 : 0;
  meta.recordCount = obj.recordCount ? obj.recordCount[0]._previousDataValues.recordCount || 0 : 0;
  meta.pageCount = Math.floor(meta.recordCount / meta.limit) + 1;
  return {
    meta: meta,
    records: getList(obj)
  };
}

function getError(obj, errorCode) {
  return {
    developerMessage: obj.message || obj.userMessage,
    userMessage: obj.userMessage
      ? obj.userMessage
      : "Ops, ocorreu um erro inesperado! =( ",
    errorCode: errorCode
  };
}
