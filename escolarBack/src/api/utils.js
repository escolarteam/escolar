module.exports = {
  include
};

function include(model, where, required, atts, include) {
  let ret = {};
  if (model) ret.model = model;
  if (atts) ret.attributes = atts;
  if (required) ret.required = required;
  if (where) ret.where = where;
  if (include) ret.include = include;
  return ret;
}
