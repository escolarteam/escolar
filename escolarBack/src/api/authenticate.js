const md5 = require('md5'),
    webconfig = require('../../webconfig');

module.exports = (req, res) => {
    if (req.originalUrl == "/api/dçespesa" ||
        req.path == "/version" ||
        req.originalUrl == "/ping/mysql/" ||
        req.originalUrl.indexOf('/despesa/zip/') == 0 ||
        req.originalUrl.indexOf('/relatorioAp') == 0) {
        return;
    }

    let tokenInvalido = {
        success: false,
        userMessage: "Faça o login novamente!!",
        message: 'Token inválido!',
        status: 403
    };

    let header = (req.headers['usuariologado'] || req.query.usuariologado);
    if (header) {
        try {
            var decodeHeader = JSON.parse(new Buffer(header, 'base64').toString());
            var hash = md5(md5(JSON.stringify(decodeHeader.user)) + md5(webconfig.secretWord));
            if (hash != decodeHeader.token)
                throw tokenInvalido;
            return;
        } catch (err) {
            throw tokenInvalido;
        }
    } else {
        throw tokenInvalido;
    }
}