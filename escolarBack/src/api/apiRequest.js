const request = require('request-promise');

module.exports = async (options) => {
    return validate(await request(options));
};

let erro = {
    success: false,
    userMessage: 'Ocorreu alguma falha na comunicação com alguma api, tente novamente.',
    message: '',
    status: 500
};

function validate (obj){
    obj = tryJson(obj);
    if (!obj.statusCode) {
        return obj;
    }

    erro.status = obj.statusCode || 500;

    if (obj) {
        if (obj.code == 'ECONNREFUSED') {
            erro.message = `A conexão foi recusada pelo servidor ${obj.address}:${obj.port}, aparentemente está off-line.`;
        }
        else {
            erro.message = `Ocorreu alguma falha na comunicação com alguma api, tente novamente.`;
        }
    } else {
        erro.message = `Ocorreu alguma falha na comunicação com alguma api, tente novamente.`;
    }
    throw erro;
}

function tryJson(content) {
    try {
        return JSON.parse(content);
    } catch (ex) {
        erro.message = 'O Content Type esperado é diferente do formato JSON';
        throw erro;
    }
}