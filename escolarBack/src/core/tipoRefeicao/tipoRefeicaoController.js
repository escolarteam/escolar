const tipoRefeicaoRepository = require("./tipoRefeicaoRepository");
//   tipoRefeicaoService = require("./tipoRefeicaoService");

module.exports = {
  get,
  getById,
  insert,
  update,
  exclude
};

async function get(req, pg, recordCount) {
  let ret = await tipoRefeicaoRepository.get(pg);
  ret.recordCount = await tipoRefeicaoRepository.get({}, recordCount);  
  return ret;
}

async function getById(req) {
  return await tipoRefeicaoRepository.getById(req.params.id);
}

async function insert(req) {
  return await tipoRefeicaoRepository.insert(req.body.tipoRefeicao);
}

async function update(req) {
  return await tipoRefeicaoRepository.update(req.body.tipoRefeicao);
}

async function exclude(req) {
  return await tipoRefeicaoRepository.exclude(req.params.id);
}
