const tipoRefeicao = require('./tipoRefeicaoModel')();

module.exports = {
    get,
    getById,
    insert,
    update,
    exclude
}

async function get(pg, recordCount) {
    return await tipoRefeicao.findAll({offset: pg.offset, limit: pg.limit, attributes: recordCount });
}

async function getById(id) {
    return await tipoRefeicao.findAll({ where: { ID: id } });
}

async function insert(tipoRefeicaoModel, transaction) {
    return await tipoRefeicao.create(tipoRefeicaoModel, { transaction });
}

async function update(tipoRefeicaoModel, transaction) {
    return await tipoRefeicao.update(tipoRefeicaoModel, { transaction, where: { ID: tipoRefeicaoModel.ID } });
}

async function exclude(id, transaction) {
    return await tipoRefeicao.destroy({ transaction, where: { ID: id } });
}

