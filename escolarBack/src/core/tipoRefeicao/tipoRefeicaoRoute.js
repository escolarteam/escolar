const controller = require('./tipoRefeicaoController');

module.exports = [{
    verbo: 'get',
    rota: '/api/tipoRefeicao/',
    metodo: controller.get,
    public: true
}, {
    verbo: 'get',
    rota: '/api/tipoRefeicao/:id',
    metodo: controller.getById,
    public: true
}, {
    verbo: 'post',
    rota: '/api/tipoRefeicao/',
    metodo: controller.insert,
    public: true
}, {
    verbo: 'put',
    rota: '/api/tipoRefeicao/',
    metodo: controller.update,
    public: true
}, {
    verbo: 'delete',
    rota: '/api/tipoRefeicao/:id',
    metodo: controller.exclude,
    public: true
}
];