const tipoAtividade = require('../tipoAtividade/tipoAtividadeModel')(),
    planoAula = require('../planoAula/planoAulaModel')(),
    sequelizeMYSQL = require('../../../config/sequelizeConfig'),
    sequelize = sequelizeMYSQL.sequelize(),
    DataTypes = sequelizeMYSQL.Sequelize.DataTypes;

module.exports = () => {
    return atividade;
}

const atividade = sequelize.define('atividade', {
    ID: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    CREATED_BY: {
        type: DataTypes.STRING,
        allowNull: false
    },
    CREATED_ON: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW
    },
    ALTER_BY: {
        type: DataTypes.STRING,
        allowNull: false
    },
    ALTER_ON: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW
    },
    DESCRICAO: {
        type: DataTypes.STRING,
        allowNull: false
    },
    OBJETIVO: {
        type: DataTypes.STRING,
        allowNull: false
    },
    HORARIO: {
        type: DataTypes.DATE,
        allowNull: false
    },
    TIPO_ATIVIDADE: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    PLANO_AULA: {
        type: DataTypes.INTEGER,
        allowNull: false
    }
},
    {
        tableName: "atividade",
        timestamps: false
    }
);

atividade.belongsTo(planoAula, { foreignKey: 'PLANO_AULA', targetKey: 'ID' });
atividade.belongsTo(tipoAtividade, { foreignKey: 'TIPO_ATIVIDADE', targetKey: 'ID' });
