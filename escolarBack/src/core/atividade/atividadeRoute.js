const controller = require('./atividadeController');

module.exports = [{
    verbo: 'get',
    rota: '/api/atividade/',
    metodo: controller.get,
    public: true
}, {
    verbo: 'get',
    rota: '/api/atividade/:id',
    metodo: controller.getById,
    public: true
}, {
    verbo: 'post',
    rota: '/api/atividade/',
    metodo: controller.insert,
    public: true
}, {
    verbo: 'put',
    rota: '/api/atividade/',
    metodo: controller.update,
    public: true
}, {
    verbo: 'delete',
    rota: '/api/atividade/:id',
    metodo: controller.exclude,
    public: true
}
];