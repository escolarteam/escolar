const atividade = require('./atividadeModel')();

module.exports = {
    get,
    getById,
    insert,
    update,
    exclude
}

async function get(pg, recordCount) {
    return await atividade.findAll({offset: pg.offset, limit: pg.limit, attributes: recordCount });
}

async function getById(id) {
    return await atividade.findAll({ where: { ID: id } });
}

async function insert(atividadeModel, transaction) {
    return await atividade.create(atividadeModel, { transaction });
}

async function update(atividadeModel, transaction) {
    return await atividade.update(atividadeModel, { transaction, where: { ID: atividadeModel.ID } });
}

async function exclude(id, transaction) {
    return await atividade.destroy({ transaction, where: { ID: id } });
}

