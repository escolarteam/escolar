const atividadeRepository = require("./atividadeRepository");
//   atividadeService = require("./atividadeService");

module.exports = {
  get,
  getById,
  insert,
  update,
  exclude
};

async function get(req, pg, recordCount) {
  let ret = await atividadeRepository.get(pg);
  ret.recordCount = await atividadeRepository.get({}, recordCount);  
  return ret;
}

async function getById(req) {
  return await atividadeRepository.getById(req.params.id);
}

async function insert(req) {
  return await atividadeRepository.insert(req.body.atividade);
}

async function update(req) {
  return await atividadeRepository.update(req.body.atividade);
}

async function exclude(req) {
  return await atividadeRepository.exclude(req.params.id);
}
