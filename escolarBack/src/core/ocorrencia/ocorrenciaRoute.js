const controller = require('./ocorrenciaController');

module.exports = [{
    verbo: 'get',
    rota: '/api/ocorrencia/',
    metodo: controller.get,
    public: true
}, {
    verbo: 'get',
    rota: '/api/ocorrencia/:id',
    metodo: controller.getById,
    public: true
}, {
    verbo: 'post',
    rota: '/api/ocorrencia/',
    metodo: controller.insert,
    public: true
}, {
    verbo: 'put',
    rota: '/api/ocorrencia/',
    metodo: controller.update,
    public: true
}, {
    verbo: 'delete',
    rota: '/api/ocorrencia/:id',
    metodo: controller.exclude,
    public: true
}
];