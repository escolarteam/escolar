const ocorrenciaRepository = require("./ocorrenciaRepository");
//   ocorrenciaService = require("./ocorrenciaService");

module.exports = {
  get,
  getById,
  insert,
  update,
  exclude
};

async function get(req, pg, recordCount) {
  let ret = await ocorrenciaRepository.get(pg);
  ret.recordCount = await ocorrenciaRepository.get({}, recordCount);  
  return ret;
}

async function getById(req) {
  return await ocorrenciaRepository.getById(req.params.id);
}

async function insert(req) {
  return await ocorrenciaRepository.insert(req.body.ocorrencia);
}

async function update(req) {
  return await ocorrenciaRepository.update(req.body.ocorrencia);
}

async function exclude(req) {
  return await ocorrenciaRepository.exclude(req.params.id);
}
