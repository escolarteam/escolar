const sequelizeMYSQL = require('../../../config/sequelizeConfig'),
    tipoOcorrencia = require('../tipoOcorrencia/tipoOcorrenciaModel')(),
    sequelize = sequelizeMYSQL.sequelize(),
    DataTypes = sequelizeMYSQL.Sequelize.DataTypes;

module.exports = () => {
    return ocorrencia;
}

const ocorrencia = sequelize.define('ocorrencia', {
    ID: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    CREATED_BY: {
        type: DataTypes.STRING,
        allowNull: false
    },
    CREATED_ON: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW
    },
    ALTER_BY: {
        type: DataTypes.STRING,
        allowNull: false
    },
    ALTER_ON: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW
    },
    DESCRICAO: {
        type: DataTypes.STRING,
        allowNull: false
    },
    TIPO_OCORRENCIA: {
        type: DataTypes.INTEGER,
        allowNull: false
    }
},
    {
        tableName: "ocorrencia",
        timestamps: false
    }
);

ocorrencia.belongsTo(tipoOcorrencia, { foreignKey: 'TIPO_OCORRENCIA', targetKey: 'ID' });