const ocorrencia = require('./ocorrenciaModel')();

module.exports = {
    get,
    getById,
    insert,
    update,
    exclude
}

async function get(pg, recordCount) {
    return await ocorrencia.findAll({offset: pg.offset, limit: pg.limit, attributes: recordCount });
}

async function getById(id) {
    return await ocorrencia.findAll({ where: { ID: id } });
}

async function insert(ocorrenciaModel, transaction) {
    return await ocorrencia.create(ocorrenciaModel, { transaction });
}

async function update(ocorrenciaModel, transaction) {
    return await ocorrencia.update(ocorrenciaModel, { transaction, where: { ID: ocorrenciaModel.ID } });
}

async function exclude(id, transaction) {
    return await ocorrencia.destroy({ transaction, where: { ID: id } });
}

