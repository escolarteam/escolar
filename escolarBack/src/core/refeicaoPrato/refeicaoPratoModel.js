const prato = require('../prato/pratoModel')(),
    refeicao = require('../refeicao/refeicaoModel')(),
    sequelizeMYSQL = require('../../../config/sequelizeConfig'),
    sequelize = sequelizeMYSQL.sequelize(),
    DataTypes = sequelizeMYSQL.Sequelize.DataTypes;

module.exports = () => {
    return refeicaoPrato;
}

const refeicaoPrato = sequelize.define('refeicaoPrato', {
    ID: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    CREATED_BY: {
        type: DataTypes.STRING,
        allowNull: false
    },
    CREATED_ON: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW
    },
    ALTER_BY: {
        type: DataTypes.STRING,
        allowNull: false
    },
    ALTER_ON: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW
    },
    PRATO: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    REFEICAO: {
        type: DataTypes.INTEGER,
        allowNull: false
    }
},
    {
        tableName: "refeicao_prato",
        timestamps: false
    }
);

refeicaoPrato.belongsTo(prato, { foreignKey: 'PRATO', targetKey: 'ID' });
refeicaoPrato.belongsTo(refeicao, { foreignKey: 'REFEICAO', targetKey: 'ID' });
