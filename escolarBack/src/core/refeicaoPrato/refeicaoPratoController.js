const refeicaoPratoRepository = require("./refeicaoPratoRepository");
//   refeicaoPratoService = require("./refeicaoPratoService");

module.exports = {
  get,
  getById,
  insert,
  update,
  exclude
};

async function get(req, pg, recordCount) {
  let ret = await refeicaoPratoRepository.get(pg);
  ret.recordCount = await refeicaoPratoRepository.get({}, recordCount);  
  return ret;
}

async function getById(req) {
  return await refeicaoPratoRepository.getById(req.params.id);
}

async function insert(req) {
  return await refeicaoPratoRepository.insert(req.body.refeicaoPrato);
}

async function update(req) {
  return await refeicaoPratoRepository.update(req.body.refeicaoPrato);
}

async function exclude(req) {
  return await refeicaoPratoRepository.exclude(req.params.id);
}
