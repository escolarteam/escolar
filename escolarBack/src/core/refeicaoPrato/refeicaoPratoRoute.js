const controller = require('./refeicaoPratoController');

module.exports = [{
    verbo: 'get',
    rota: '/api/refeicaoPrato/',
    metodo: controller.get,
    public: true
}, {
    verbo: 'get',
    rota: '/api/refeicaoPrato/:id',
    metodo: controller.getById,
    public: true
}, {
    verbo: 'post',
    rota: '/api/refeicaoPrato/',
    metodo: controller.insert,
    public: true
}, {
    verbo: 'put',
    rota: '/api/refeicaoPrato/',
    metodo: controller.update,
    public: true
}, {
    verbo: 'delete',
    rota: '/api/refeicaoPrato/:id',
    metodo: controller.exclude,
    public: true
}
];