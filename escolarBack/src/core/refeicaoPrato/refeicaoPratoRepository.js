const refeicaoPrato = require('./refeicaoPratoModel')();

module.exports = {
    get,
    getById,
    insert,
    update,
    exclude
}

async function get(pg, recordCount) {
    return await refeicaoPrato.findAll({offset: pg.offset, limit: pg.limit, attributes: recordCount });
}

async function getById(id) {
    return await refeicaoPrato.findAll({ where: { ID: id } });
}

async function insert(refeicaoPratoModel, transaction) {
    return await refeicaoPrato.create(refeicaoPratoModel, { transaction });
}

async function update(refeicaoPratoModel, transaction) {
    return await refeicaoPrato.update(refeicaoPratoModel, { transaction, where: { ID: refeicaoPratoModel.ID } });
}

async function exclude(id, transaction) {
    return await refeicaoPrato.destroy({ transaction, where: { ID: id } });
}

