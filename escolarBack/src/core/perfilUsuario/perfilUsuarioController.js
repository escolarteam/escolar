const perfilUsuarioRepository = require("./perfilUsuarioRepository");
//   perfilUsuarioService = require("./perfilUsuarioService");

module.exports = {
  get,
  getById,
  insert,
  update,
  exclude
};

async function get(req, pg, recordCount) {
  let ret = await perfilUsuarioRepository.get(pg);
  ret.recordCount = await perfilUsuarioRepository.get({}, recordCount);  
  return ret;
}

async function getById(req) {
  return await perfilUsuarioRepository.getById(req.params.id);
}

async function insert(req) {
  return await perfilUsuarioRepository.insert(req.body.perfilUsuario);
}

async function update(req) {
  return await perfilUsuarioRepository.update(req.body.perfilUsuario);
}

async function exclude(req) {
  return await perfilUsuarioRepository.exclude(req.params.id);
}
