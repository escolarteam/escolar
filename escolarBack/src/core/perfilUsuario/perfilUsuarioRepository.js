const perfilUsuario = require('./perfilUsuarioModel')();

module.exports = {
    get,
    getById,
    insert,
    update,
    exclude
}

async function get(pg, recordCount) {
    return await perfilUsuario.findAll({offset: pg.offset, limit: pg.limit, attributes: recordCount });
}

async function getById(id) {
    return await perfilUsuario.findAll({ where: { ID: id } });
}

async function insert(perfilUsuarioModel, transaction) {
    return await perfilUsuario.create(perfilUsuarioModel, { transaction });
}

async function update(perfilUsuarioModel, transaction) {
    return await perfilUsuario.update(perfilUsuarioModel, { transaction, where: { ID: perfilUsuarioModel.ID } });
}

async function exclude(id, transaction) {
    return await perfilUsuario.destroy({ transaction, where: { ID: id } });
}

