const controller = require('./perfilUsuarioController');

module.exports = [{
    verbo: 'get',
    rota: '/api/perfilUsuario/',
    metodo: controller.get,
    public: true
}, {
    verbo: 'get',
    rota: '/api/perfilUsuario/:id',
    metodo: controller.getById,
    public: true
}, {
    verbo: 'post',
    rota: '/api/perfilUsuario/',
    metodo: controller.insert,
    public: true
}, {
    verbo: 'put',
    rota: '/api/perfilUsuario/',
    metodo: controller.update,
    public: true
}, {
    verbo: 'delete',
    rota: '/api/perfilUsuario/:id',
    metodo: controller.exclude,
    public: true
}
];