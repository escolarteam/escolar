const perfil = require('../perfil/perfilModel')(),
    usuario = require('../usuario/usuarioModel')(),
    sequelizeMYSQL = require('../../../config/sequelizeConfig'),
    sequelize = sequelizeMYSQL.sequelize(),
    DataTypes = sequelizeMYSQL.Sequelize.DataTypes;

module.exports = () => {
    return perfilUsuario;
}

const perfilUsuario = sequelize.define('perfilUsuario', {
    ID: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    CREATED_BY: {
        type: DataTypes.STRING,
        allowNull: false
    },
    CREATED_ON: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW
    },
    ALTER_BY: {
        type: DataTypes.STRING,
        allowNull: false
    },
    ALTER_ON: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW
    },
    USUARIO: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    PERFIL: {
        type: DataTypes.INTEGER,
        allowNull: false
    }
},
    {
        tableName: "perfil_usuario",
        timestamps: false
    }
);

perfilUsuario.belongsTo(perfil, { foreignKey: 'PERFIL', targetKey: 'ID' });
perfilUsuario.belongsTo(usuario, { foreignKey: 'USUARIO', targetKey: 'ID' });
