const redeRepository = require("./redeRepository");
//   redeService = require("./redeService");

module.exports = {
  get,
  getById,
  insert,
  update,
  exclude
};

async function get(req, pg, recordCount) {
  let ret = await redeRepository.get(pg);
  ret.recordCount = await redeRepository.get({}, recordCount);  
  return ret;
}

async function getById(req) {
  return await redeRepository.getById(req.params.id);
}

async function insert(req) {
  return await redeRepository.insert(req.body.rede);
}

async function update(req) {
  return await redeRepository.update(req.body.rede);
}

async function exclude(req) {
  return await redeRepository.exclude(req.params.id);
}
