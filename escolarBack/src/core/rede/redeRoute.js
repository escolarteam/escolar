const controller = require('./redeController');

module.exports = [{
    verbo: 'get',
    rota: '/api/rede/',
    metodo: controller.get,
    public: true
}, {
    verbo: 'get',
    rota: '/api/rede/:id',
    metodo: controller.getById,
    public: true
}, {
    verbo: 'post',
    rota: '/api/rede/',
    metodo: controller.insert,
    public: true
}, {
    verbo: 'put',
    rota: '/api/rede/',
    metodo: controller.update,
    public: true
}, {
    verbo: 'delete',
    rota: '/api/rede/:id',
    metodo: controller.exclude,
    public: true
}
];