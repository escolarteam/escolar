const rede = require('./redeModel')();

module.exports = {
    get,
    getById,
    insert,
    update,
    exclude
}

async function get(pg, recordCount) {
    return await rede.findAll({offset: pg.offset, limit: pg.limit, attributes: recordCount });
}

async function getById(id) {
    return await rede.findAll({ where: { ID: id } });
}

async function insert(redeModel, transaction) {
    return await rede.create(redeModel, { transaction });
}

async function update(redeModel, transaction) {
    return await rede.update(redeModel, { transaction, where: { ID: redeModel.ID } });
}

async function exclude(id, transaction) {
    return await rede.destroy({ transaction, where: { ID: id } });
}

