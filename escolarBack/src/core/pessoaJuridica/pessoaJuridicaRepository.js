const pessoaJuridica = require('./pessoaJuridicaModel')();

module.exports = {
    get,
    getById,
    insert,
    update,
    exclude
}

async function get(pg, recordCount) {
    return await pessoaJuridica.findAll({offset: pg.offset, limit: pg.limit, attributes: recordCount });
}

async function getById(id) {
    return await pessoaJuridica.findAll({ where: { ID: id } });
}

async function insert(pessoaJuridicaModel, transaction) {
    return await pessoaJuridica.create(pessoaJuridicaModel, { transaction });
}

async function update(pessoaJuridicaModel, transaction) {
    return await pessoaJuridica.update(pessoaJuridicaModel, { transaction, where: { ID: pessoaJuridicaModel.ID } });
}

async function exclude(id, transaction) {
    return await pessoaJuridica.destroy({ transaction, where: { ID: id } });
}

