const controller = require('./pessoaJuridicaController');

module.exports = [{
    verbo: 'get',
    rota: '/api/pessoaJuridica/',
    metodo: controller.get,
    public: true
}, {
    verbo: 'get',
    rota: '/api/pessoaJuridica/:id',
    metodo: controller.getById,
    public: true
}, {
    verbo: 'post',
    rota: '/api/pessoaJuridica/',
    metodo: controller.insert,
    public: true
}, {
    verbo: 'put',
    rota: '/api/pessoaJuridica/',
    metodo: controller.update,
    public: true
}, {
    verbo: 'delete',
    rota: '/api/pessoaJuridica/:id',
    metodo: controller.exclude,
    public: true
}
];