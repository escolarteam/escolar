const pessoa = require('../pessoa/pessoaModel')(), 
    sequelizeMYSQL = require('../../../config/sequelizeConfig'),
    sequelize = sequelizeMYSQL.sequelize(),
    DataTypes = sequelizeMYSQL.Sequelize.DataTypes;

module.exports = () => {
    return pessoaJuridica;
}

const pessoaJuridica = sequelize.define('pessoaJuridica', {
    ID: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    CREATED_BY: {
        type: DataTypes.STRING,
        allowNull: false
    },
    CREATED_ON: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW
    },
    ALTER_BY: {
        type: DataTypes.STRING,
        allowNull: false
    },
    ALTER_ON: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW
    },
    CNPJ: {
        type: DataTypes.STRING,
        allowNull: false
    },
    INCRICAO_MUNICIPAL: {
        type: DataTypes.STRING,
        allowNull: false
    },
    INCRICAO_ESTADUAL: {
        type: DataTypes.STRING,
        allowNull: false
    },
    NOME_FANTASIA: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    RAZAO_SOCIAL: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    PESSOA: {
        type: DataTypes.INTEGER,
        allowNull: false
    }
},
    {
        tableName: "pessoa_juridica",
        timestamps: false
    }
);

pessoaJuridica.belongsTo(pessoa, { foreignKey: 'PESSOA', targetKey: 'ID' });
