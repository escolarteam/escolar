const pessoaJuridicaRepository = require("./pessoaJuridicaRepository");
//   pessoaJuridicaService = require("./pessoaJuridicaService");

module.exports = {
  get,
  getById,
  insert,
  update,
  exclude
};

async function get(req, pg, recordCount) {
  let ret = await pessoaJuridicaRepository.get(pg);
  ret.recordCount = await pessoaJuridicaRepository.get({}, recordCount);  
  return ret;
}

async function getById(req) {
  return await pessoaJuridicaRepository.getById(req.params.id);
}

async function insert(req) {
  return await pessoaJuridicaRepository.insert(req.body.pessoaJuridica);
}

async function update(req) {
  return await pessoaJuridicaRepository.update(req.body.pessoaJuridica);
}

async function exclude(req) {
  return await pessoaJuridicaRepository.exclude(req.params.id);
}
