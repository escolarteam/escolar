const tipoOcorrenciaRepository = require("./tipoOcorrenciaRepository");
//   tipoOcorrenciaService = require("./tipoOcorrenciaService");

module.exports = {
  get,
  getById,
  insert,
  update,
  exclude
};

async function get(req, pg, recordCount) {
  let ret = await tipoOcorrenciaRepository.get(pg);
  ret.recordCount = await tipoOcorrenciaRepository.get({}, recordCount);  
  return ret;
}

async function getById(req) {
  return await tipoOcorrenciaRepository.getById(req.params.id);
}

async function insert(req) {
  return await tipoOcorrenciaRepository.insert(req.body.tipoOcorrencia);
}

async function update(req) {
  return await tipoOcorrenciaRepository.update(req.body.tipoOcorrencia);
}

async function exclude(req) {
  return await tipoOcorrenciaRepository.exclude(req.params.id);
}
