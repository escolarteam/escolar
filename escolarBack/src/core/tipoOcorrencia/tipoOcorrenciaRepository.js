const tipoOcorrencia = require('./tipoOcorrenciaModel')();

module.exports = {
    get,
    getById,
    insert,
    update,
    exclude
}

async function get(pg, recordCount) {
    return await tipoOcorrencia.findAll({offset: pg.offset, limit: pg.limit, attributes: recordCount });
}

async function getById(id) {
    return await tipoOcorrencia.findAll({ where: { ID: id } });
}

async function insert(tipoOcorrenciaModel, transaction) {
    return await tipoOcorrencia.create(tipoOcorrenciaModel, { transaction });
}

async function update(tipoOcorrenciaModel, transaction) {
    return await tipoOcorrencia.update(tipoOcorrenciaModel, { transaction, where: { ID: tipoOcorrenciaModel.ID } });
}

async function exclude(id, transaction) {
    return await tipoOcorrencia.destroy({ transaction, where: { ID: id } });
}

