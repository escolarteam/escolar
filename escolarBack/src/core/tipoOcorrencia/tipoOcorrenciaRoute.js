const controller = require('./tipoOcorrenciaController');

module.exports = [{
    verbo: 'get',
    rota: '/api/tipoOcorrencia/',
    metodo: controller.get,
    public: true
}, {
    verbo: 'get',
    rota: '/api/tipoOcorrencia/:id',
    metodo: controller.getById,
    public: true
}, {
    verbo: 'post',
    rota: '/api/tipoOcorrencia/',
    metodo: controller.insert,
    public: true
}, {
    verbo: 'put',
    rota: '/api/tipoOcorrencia/',
    metodo: controller.update,
    public: true
}, {
    verbo: 'delete',
    rota: '/api/tipoOcorrencia/:id',
    metodo: controller.exclude,
    public: true
}
];