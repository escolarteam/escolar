const cardapioRefeicaoRepository = require("./cardapioRefeicaoRepository");
//   cardapioRefeicaoService = require("./cardapioRefeicaoService");

module.exports = {
  get,
  getById,
  insert,
  update,
  exclude
};

async function get(req, pg, recordCount) {
  let ret = await cardapioRefeicaoRepository.get(pg);
  ret.recordCount = await cardapioRefeicaoRepository.get({}, recordCount);  
  return ret;
}

async function getById(req) {
  return await cardapioRefeicaoRepository.getById(req.params.id);
}

async function insert(req) {
  return await cardapioRefeicaoRepository.insert(req.body.cardapioRefeicao);
}

async function update(req) {
  return await cardapioRefeicaoRepository.update(req.body.cardapioRefeicao);
}

async function exclude(req) {
  return await cardapioRefeicaoRepository.exclude(req.params.id);
}
