const cardapio = require('../cardapio/cardapioModel')(),
    refeicao = require('../refeicao/refeicaoModel')(),
    sequelizeMYSQL = require('../../../config/sequelizeConfig'),
    sequelize = sequelizeMYSQL.sequelize(),
    DataTypes = sequelizeMYSQL.Sequelize.DataTypes;

module.exports = () => {
    return cardapioRefeicao;
}

const cardapioRefeicao = sequelize.define('cardapioRefeicao', {
    ID: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    CREATED_BY: {
        type: DataTypes.STRING,
        allowNull: false
    },
    CREATED_ON: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW
    },
    ALTER_BY: {
        type: DataTypes.STRING,
        allowNull: false
    },
    ALTER_ON: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW
    },
    CARDAPIO: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    REFEICAO: {
        type: DataTypes.INTEGER,
        allowNull: false
    }
},
    {
        tableName: "cardapio_refeicao",
        timestamps: false
    }
);

cardapioRefeicao.belongsTo(cardapio, { foreignKey: 'CARDAPIO', targetKey: 'ID' });
cardapioRefeicao.belongsTo(refeicao, { foreignKey: 'REFEICAO', targetKey: 'ID' });
