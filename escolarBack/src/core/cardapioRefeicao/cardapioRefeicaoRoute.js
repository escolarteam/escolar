const controller = require('./cardapioRefeicaoController');

module.exports = [{
    verbo: 'get',
    rota: '/api/cardapioRefeicao/',
    metodo: controller.get,
    public: true
}, {
    verbo: 'get',
    rota: '/api/cardapioRefeicao/:id',
    metodo: controller.getById,
    public: true
}, {
    verbo: 'post',
    rota: '/api/cardapioRefeicao/',
    metodo: controller.insert,
    public: true
}, {
    verbo: 'put',
    rota: '/api/cardapioRefeicao/',
    metodo: controller.update,
    public: true
}, {
    verbo: 'delete',
    rota: '/api/cardapioRefeicao/:id',
    metodo: controller.exclude,
    public: true
}
];