const cardapioRefeicao = require('./cardapioRefeicaoModel')();

module.exports = {
    get,
    getById,
    insert,
    update,
    exclude
}

async function get(pg, recordCount) {
    return await cardapioRefeicao.findAll({offset: pg.offset, limit: pg.limit, attributes: recordCount });
}

async function getById(id) {
    return await cardapioRefeicao.findAll({ where: { ID: id } });
}

async function insert(cardapioRefeicaoModel, transaction) {
    return await cardapioRefeicao.create(cardapioRefeicaoModel, { transaction });
}

async function update(cardapioRefeicaoModel, transaction) {
    return await cardapioRefeicao.update(cardapioRefeicaoModel, { transaction, where: { ID: cardapioRefeicaoModel.ID } });
}

async function exclude(id, transaction) {
    return await cardapioRefeicao.destroy({ transaction, where: { ID: id } });
}

