const controller = require('./enderecoController');

module.exports = [{
    verbo: 'get',
    rota: '/api/endereco/',
    metodo: controller.get,
    public: true
}, {
    verbo: 'get',
    rota: '/api/endereco/:id',
    metodo: controller.getById,
    public: true
}, {
    verbo: 'post',
    rota: '/api/endereco/',
    metodo: controller.insert,
    public: true
}, {
    verbo: 'put',
    rota: '/api/endereco/',
    metodo: controller.update,
    public: true
}, {
    verbo: 'delete',
    rota: '/api/endereco/:id',
    metodo: controller.exclude,
    public: true
}
];