const sequelizeMYSQL = require('../../../config/sequelizeConfig'),
    sequelize = sequelizeMYSQL.sequelize(),
    DataTypes = sequelizeMYSQL.Sequelize.DataTypes;

module.exports = () => {
    return endereco;
}

const endereco = sequelize.define('endereco', {
    ID: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    LOGRADOURO: {
        type: DataTypes.STRING,
        allowNull: false
    },
    NUMERO: {
        type: DataTypes.STRING,
        allowNull: false
    },
    BAIRRO: {
        type: DataTypes.STRING,
        allowNull: false
    },
    COMPLEMENTO: {
        type: DataTypes.STRING,
        allowNull: false
    },
    CEP: {
        type: DataTypes.STRING,
        allowNull: false
    },
    CIDADE: {
        type: DataTypes.STRING,
        allowNull: false
    },
    UF: {
        type: DataTypes.STRING,
        allowNull: false
    },
    PAIS: {
        type: DataTypes.STRING,
        allowNull: false
    }
},
    {
        tableName: "endereco",
        timestamps: false
    }
);

