const enderecoRepository = require("./enderecoRepository");
//   enderecoService = require("./enderecoService");

module.exports = {
  get,
  getById,
  insert,
  update,
  exclude
};

async function get(req, pg, recordCount) {
  let ret = await enderecoRepository.get(pg);
  ret.recordCount = await enderecoRepository.get({}, recordCount);  
  return ret;
}

async function getById(req) {
  return await enderecoRepository.getById(req.params.id);
}

async function insert(req) {
  return await enderecoRepository.insert(req.body.endereco);
}

async function update(req) {
  return await enderecoRepository.update(req.body.endereco);
}

async function exclude(req) {
  return await enderecoRepository.exclude(req.params.id);
}
