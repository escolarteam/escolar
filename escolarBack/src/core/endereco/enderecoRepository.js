const endereco = require('./enderecoModel')();

module.exports = {
    get,
    getById,
    insert,
    update,
    exclude
}

async function get(pg, recordCount) {
    return await endereco.findAll({offset: pg.offset, limit: pg.limit, attributes: recordCount });
}

async function getById(id) {
    return await endereco.findAll({ where: { ID: id } });
}

async function insert(enderecoModel, transaction) {
    return await endereco.create(enderecoModel, { transaction });
}

async function update(enderecoModel, transaction) {
    return await endereco.update(enderecoModel, { transaction, where: { ID: enderecoModel.ID } });
}

async function exclude(id, transaction) {
    return await endereco.destroy({ transaction, where: { ID: id } });
}

