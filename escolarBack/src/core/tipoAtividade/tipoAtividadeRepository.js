const tipoAtividade = require('./tipoAtividadeModel')();

module.exports = {
    get,
    getById,
    insert,
    update,
    exclude
}

async function get(pg, recordCount) {
    return await tipoAtividade.findAll({offset: pg.offset, limit: pg.limit, attributes: recordCount });
}

async function getById(id) {
    return await tipoAtividade.findAll({ where: { ID: id } });
}

async function insert(tipoAtividadeModel, transaction) {
    return await tipoAtividade.create(tipoAtividadeModel, { transaction });
}

async function update(tipoAtividadeModel, transaction) {
    return await tipoAtividade.update(tipoAtividadeModel, { transaction, where: { ID: tipoAtividadeModel.ID } });
}

async function exclude(id, transaction) {
    return await tipoAtividade.destroy({ transaction, where: { ID: id } });
}

