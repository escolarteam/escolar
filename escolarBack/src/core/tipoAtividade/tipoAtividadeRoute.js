const controller = require('./tipoAtividadeController');

module.exports = [{
    verbo: 'get',
    rota: '/api/tipoAtividade/',
    metodo: controller.get,
    public: true
}, {
    verbo: 'get',
    rota: '/api/tipoAtividade/:id',
    metodo: controller.getById,
    public: true
}, {
    verbo: 'post',
    rota: '/api/tipoAtividade/',
    metodo: controller.insert,
    public: true
}, {
    verbo: 'put',
    rota: '/api/tipoAtividade/',
    metodo: controller.update,
    public: true
}, {
    verbo: 'delete',
    rota: '/api/tipoAtividade/:id',
    metodo: controller.exclude,
    public: true
}
];