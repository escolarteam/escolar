const tipoAtividadeRepository = require("./tipoAtividadeRepository");
//   tipoAtividadeService = require("./tipoAtividadeService");

module.exports = {
  get,
  getById,
  insert,
  update,
  exclude
};

async function get(req, pg, recordCount) {
  let ret = await tipoAtividadeRepository.get(pg);
  ret.recordCount = await tipoAtividadeRepository.get({}, recordCount);  
  return ret;
}

async function getById(req) {
  return await tipoAtividadeRepository.getById(req.params.id);
}

async function insert(req) {
  return await tipoAtividadeRepository.insert(req.body.tipoAtividade);
}

async function update(req) {
  return await tipoAtividadeRepository.update(req.body.tipoAtividade);
}

async function exclude(req) {
  return await tipoAtividadeRepository.exclude(req.params.id);
}
