const controller = require('./pratoController');

module.exports = [{
    verbo: 'get',
    rota: '/api/prato/',
    metodo: controller.get,
    public: true
}, {
    verbo: 'get',
    rota: '/api/prato/:id',
    metodo: controller.getById,
    public: true
}, {
    verbo: 'post',
    rota: '/api/prato/',
    metodo: controller.insert,
    public: true
}, {
    verbo: 'put',
    rota: '/api/prato/',
    metodo: controller.update,
    public: true
}, {
    verbo: 'delete',
    rota: '/api/prato/:id',
    metodo: controller.exclude,
    public: true
}
];