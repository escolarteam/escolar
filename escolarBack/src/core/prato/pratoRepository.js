const prato = require('./pratoModel')();

module.exports = {
    get,
    getById,
    insert,
    update,
    exclude
}

async function get(pg, recordCount) {
    return await prato.findAll({offset: pg.offset, limit: pg.limit, attributes: recordCount });
}

async function getById(id) {
    return await prato.findAll({ where: { ID: id } });
}

async function insert(pratoModel, transaction) {
    return await prato.create(pratoModel, { transaction });
}

async function update(pratoModel, transaction) {
    return await prato.update(pratoModel, { transaction, where: { ID: pratoModel.ID } });
}

async function exclude(id, transaction) {
    return await prato.destroy({ transaction, where: { ID: id } });
}

