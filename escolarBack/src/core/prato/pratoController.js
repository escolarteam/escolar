const pratoRepository = require("./pratoRepository");
//   pratoService = require("./pratoService");

module.exports = {
  get,
  getById,
  insert,
  update,
  exclude
};

async function get(req, pg, recordCount) {
  let ret = await pratoRepository.get(pg);
  ret.recordCount = await pratoRepository.get({}, recordCount);  
  return ret;
}

async function getById(req) {
  return await pratoRepository.getById(req.params.id);
}

async function insert(req) {
  return await pratoRepository.insert(req.body.prato);
}

async function update(req) {
  return await pratoRepository.update(req.body.prato);
}

async function exclude(req) {
  return await pratoRepository.exclude(req.params.id);
}
