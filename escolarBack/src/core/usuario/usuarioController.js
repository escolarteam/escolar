const usuarioRepository = require("./usuarioRepository");
//   usuarioService = require("./usuarioService");

module.exports = {
  get,
  getById,
  insert,
  update,
  exclude
};

async function get(req, pg, recordCount) {
  let ret = await usuarioRepository.get(pg);
  ret.recordCount = await usuarioRepository.get({}, recordCount);  
  return ret;
}

async function getById(req) {
  return await usuarioRepository.getById(req.params.id);
}

async function insert(req) {
  return await usuarioRepository.insert(req.body.usuario);
}

async function update(req) {
  return await usuarioRepository.update(req.body.usuario);
}

async function exclude(req) {
  return await usuarioRepository.exclude(req.params.id);
}
