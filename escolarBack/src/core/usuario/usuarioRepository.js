const usuario = require('./usuarioModel')();

module.exports = {
    get,
    getById,
    insert,
    update,
    exclude
}

async function get(pg, recordCount) {
    return await usuario.findAll({offset: pg.offset, limit: pg.limit, attributes: recordCount });
}

async function getById(id) {
    return await usuario.findAll({ where: { ID: id } });
}

async function insert(usuarioModel, transaction) {
    return await usuario.create(usuarioModel, { transaction });
}

async function update(usuarioModel, transaction) {
    return await usuario.update(usuarioModel, { transaction, where: { ID: usuarioModel.ID } });
}

async function exclude(id, transaction) {
    return await usuario.destroy({ transaction, where: { ID: id } });
}

