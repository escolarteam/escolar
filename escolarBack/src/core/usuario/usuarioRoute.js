const controller = require('./usuarioController');

module.exports = [{
    verbo: 'get',
    rota: '/api/usuario/',
    metodo: controller.get,
    public: true
}, {
    verbo: 'get',
    rota: '/api/usuario/:id',
    metodo: controller.getById,
    public: true
}, {
    verbo: 'post',
    rota: '/api/usuario/',
    metodo: controller.insert,
    public: true
}, {
    verbo: 'put',
    rota: '/api/usuario/',
    metodo: controller.update,
    public: true
}, {
    verbo: 'delete',
    rota: '/api/usuario/:id',
    metodo: controller.exclude,
    public: true
}
];