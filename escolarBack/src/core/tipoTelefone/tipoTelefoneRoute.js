const controller = require('./tipoTelefoneController');

module.exports = [{
    verbo: 'get',
    rota: '/api/tipoTelefone/',
    metodo: controller.get,
    public: true
}, {
    verbo: 'get',
    rota: '/api/tipoTelefone/:id',
    metodo: controller.getById,
    public: true
}, {
    verbo: 'post',
    rota: '/api/tipoTelefone/',
    metodo: controller.insert,
    public: true
}, {
    verbo: 'put',
    rota: '/api/tipoTelefone/',
    metodo: controller.update,
    public: true
}, {
    verbo: 'delete',
    rota: '/api/tipoTelefone/:id',
    metodo: controller.exclude,
    public: true
}
];