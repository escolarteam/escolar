const tipoEmail = require('./tipoEmailModel')();

module.exports = {
    get,
    getById,
    insert,
    update,
    exclude
}

async function get(pg, recordCount) {
    return await tipoEmail.findAll({offset: pg.offset, limit: pg.limit, attributes: recordCount });
}

async function getById(id) {
    return await tipoEmail.findAll({ where: { ID: id } });
}

async function insert(tipoEmailModel, transaction) {
    return await tipoEmail.create(tipoEmailModel, { transaction });
}

async function update(tipoEmailModel, transaction) {
    return await tipoEmail.update(tipoEmailModel, { transaction, where: { ID: tipoEmailModel.ID } });
}

async function exclude(id, transaction) {
    return await tipoEmail.destroy({ transaction, where: { ID: id } });
}

