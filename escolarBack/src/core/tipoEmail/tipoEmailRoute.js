const controller = require('./tipoEmailController');

module.exports = [{
    verbo: 'get',
    rota: '/api/tipoEmail/',
    metodo: controller.get,
    public: true
}, {
    verbo: 'get',
    rota: '/api/tipoEmail/:id',
    metodo: controller.getById,
    public: true
}, {
    verbo: 'post',
    rota: '/api/tipoEmail/',
    metodo: controller.insert,
    public: true
}, {
    verbo: 'put',
    rota: '/api/tipoEmail/',
    metodo: controller.update,
    public: true
}, {
    verbo: 'delete',
    rota: '/api/tipoEmail/:id',
    metodo: controller.exclude,
    public: true
}
];