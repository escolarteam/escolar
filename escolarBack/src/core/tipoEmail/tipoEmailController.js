const tipoEmailRepository = require("./tipoEmailRepository");
//   tipoEmailService = require("./tipoEmailService");

module.exports = {
  get,
  getById,
  insert,
  update,
  exclude
};

async function get(req, pg, recordCount) {
  let ret = await tipoEmailRepository.get(pg);
  ret.recordCount = await tipoEmailRepository.get({}, recordCount);  
  return ret;
}

async function getById(req) {
  return await tipoEmailRepository.getById(req.params.id);
}

async function insert(req) {
  return await tipoEmailRepository.insert(req.body.tipoEmail);
}

async function update(req) {
  return await tipoEmailRepository.update(req.body.tipoEmail);
}

async function exclude(req) {
  return await tipoEmailRepository.exclude(req.params.id);
}
