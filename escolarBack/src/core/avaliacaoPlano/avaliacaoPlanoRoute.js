const controller = require('./avaliacaoPlanoController');

module.exports = [{
    verbo: 'get',
    rota: '/api/avaliacaoPlano/',
    metodo: controller.get,
    public: true
}, {
    verbo: 'get',
    rota: '/api/avaliacaoPlano/:id',
    metodo: controller.getById,
    public: true
}, {
    verbo: 'post',
    rota: '/api/avaliacaoPlano/',
    metodo: controller.insert,
    public: true
}, {
    verbo: 'put',
    rota: '/api/avaliacaoPlano/',
    metodo: controller.update,
    public: true
}, {
    verbo: 'delete',
    rota: '/api/avaliacaoPlano/:id',
    metodo: controller.exclude,
    public: true
}
];