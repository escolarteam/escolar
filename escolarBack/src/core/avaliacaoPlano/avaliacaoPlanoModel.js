const sequelizeMYSQL = require('../../../config/sequelizeConfig'),
    pessoa = require('../pessoa/pessoaModel')(),
    planoAula = require('../planoAula/planoAulaModel')(),
    sequelize = sequelizeMYSQL.sequelize(),
    DataTypes = sequelizeMYSQL.Sequelize.DataTypes;

module.exports = () => {
    return avaliacaoPlano;
}

const avaliacaoPlano = sequelize.define('avaliacaoPlano', {
    ID: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    CREATED_BY: {
        type: DataTypes.STRING,
        allowNull: false
    },
    CREATED_ON: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW
    },
    ALTER_BY: {
        type: DataTypes.STRING,
        allowNull: false
    },
    ALTER_ON: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW
    },
    DESCRICAO: {
        type: DataTypes.STRING,
        allowNull: false
    },
    PESSOA: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    PLANO_AULA: {
        type: DataTypes.INTEGER,
        allowNull: false
    }
},
    {
        tableName: "avaliacao_plano",
        timestamps: false
    }
);

avaliacaoPlano.belongsTo(planoAula, { foreignKey: 'PLANO_AULA', targetKey: 'ID' });
avaliacaoPlano.belongsTo(pessoa, { foreignKey: 'PESSOA', targetKey: 'ID' });
