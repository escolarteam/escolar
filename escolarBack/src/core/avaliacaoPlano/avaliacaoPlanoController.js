const avaliacaoPlanoRepository = require("./avaliacaoPlanoRepository");
//   avaliacaoPlanoService = require("./avaliacaoPlanoService");

module.exports = {
  get,
  getById,
  insert,
  update,
  exclude
};

async function get(req, pg, recordCount) {
  let ret = await avaliacaoPlanoRepository.get(pg);
  ret.recordCount = await avaliacaoPlanoRepository.get({}, recordCount);  
  return ret;
}

async function getById(req) {
  return await avaliacaoPlanoRepository.getById(req.params.id);
}

async function insert(req) {
  return await avaliacaoPlanoRepository.insert(req.body.avaliacaoPlano);
}

async function update(req) {
  return await avaliacaoPlanoRepository.update(req.body.avaliacaoPlano);
}

async function exclude(req) {
  return await avaliacaoPlanoRepository.exclude(req.params.id);
}
