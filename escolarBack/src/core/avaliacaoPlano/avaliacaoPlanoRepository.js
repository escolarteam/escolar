const avaliacaoPlano = require('./avaliacaoPlanoModel')();

module.exports = {
    get,
    getById,
    insert,
    update,
    exclude
}

async function get(pg, recordCount) {
    return await avaliacaoPlano.findAll({offset: pg.offset, limit: pg.limit, attributes: recordCount });
}

async function getById(id) {
    return await avaliacaoPlano.findAll({ where: { ID: id } });
}

async function insert(avaliacaoPlanoModel, transaction) {
    return await avaliacaoPlano.create(avaliacaoPlanoModel, { transaction });
}

async function update(avaliacaoPlanoModel, transaction) {
    return await avaliacaoPlano.update(avaliacaoPlanoModel, { transaction, where: { ID: avaliacaoPlanoModel.ID } });
}

async function exclude(id, transaction) {
    return await avaliacaoPlano.destroy({ transaction, where: { ID: id } });
}

