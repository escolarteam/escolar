const pessoa = require('../pessoa/pessoaModel')(), 
    sequelizeMYSQL = require('../../../config/sequelizeConfig'),
    sequelize = sequelizeMYSQL.sequelize(),
    DataTypes = sequelizeMYSQL.Sequelize.DataTypes;

module.exports = () => {
    return planoAula;
}

const planoAula = sequelize.define('planoAula', {
    ID: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    CREATED_BY: {
        type: DataTypes.STRING,
        allowNull: false
    },
    CREATED_ON: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW
    },
    ALTER_BY: {
        type: DataTypes.STRING,
        allowNull: false
    },
    ALTER_ON: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW
    },
    DESCRICAO: {
        type: DataTypes.STRING,
        allowNull: false
    },
    PESSOA: {
        type: DataTypes.INTEGER,
        allowNull: false
    }
},
    {
        tableName: "plano_aula",
        timestamps: false
    }
);

planoAula.belongsTo(pessoa, { foreignKey: 'PESSOA', targetKey: 'ID' });
