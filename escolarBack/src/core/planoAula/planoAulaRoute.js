const controller = require('./planoAulaController');

module.exports = [{
    verbo: 'get',
    rota: '/api/planoAula/',
    metodo: controller.get,
    public: true
}, {
    verbo: 'get',
    rota: '/api/planoAula/:id',
    metodo: controller.getById,
    public: true
}, {
    verbo: 'post',
    rota: '/api/planoAula/',
    metodo: controller.insert,
    public: true
}, {
    verbo: 'put',
    rota: '/api/planoAula/',
    metodo: controller.update,
    public: true
}, {
    verbo: 'delete',
    rota: '/api/planoAula/:id',
    metodo: controller.exclude,
    public: true
}
];