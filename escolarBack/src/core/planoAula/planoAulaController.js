const planoAulaRepository = require("./planoAulaRepository");
//   planoAulaService = require("./planoAulaService");

module.exports = {
  get,
  getById,
  insert,
  update,
  exclude
};

async function get(req, pg, recordCount) {
  let ret = await planoAulaRepository.get(pg);
  ret.recordCount = await planoAulaRepository.get({}, recordCount);  
  return ret;
}

async function getById(req) {
  return await planoAulaRepository.getById(req.params.id);
}

async function insert(req) {
  return await planoAulaRepository.insert(req.body.planoAula);
}

async function update(req) {
  return await planoAulaRepository.update(req.body.planoAula);
}

async function exclude(req) {
  return await planoAulaRepository.exclude(req.params.id);
}
