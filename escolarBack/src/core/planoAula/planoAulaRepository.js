const planoAula = require('./planoAulaModel')();

module.exports = {
    get,
    getById,
    insert,
    update,
    exclude
}

async function get(pg, recordCount) {
    return await planoAula.findAll({offset: pg.offset, limit: pg.limit, attributes: recordCount });
}

async function getById(id) {
    return await planoAula.findAll({ where: { ID: id } });
}

async function insert(planoAulaModel, transaction) {
    return await planoAula.create(planoAulaModel, { transaction });
}

async function update(planoAulaModel, transaction) {
    return await planoAula.update(planoAulaModel, { transaction, where: { ID: planoAulaModel.ID } });
}

async function exclude(id, transaction) {
    return await planoAula.destroy({ transaction, where: { ID: id } });
}

