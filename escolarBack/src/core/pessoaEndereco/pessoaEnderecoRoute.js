const controller = require('./pessoaEnderecoController');

module.exports = [{
    verbo: 'get',
    rota: '/api/pessoaEndereco/',
    metodo: controller.get,
    public: true
}, {
    verbo: 'get',
    rota: '/api/pessoaEndereco/:id',
    metodo: controller.getById,
    public: true
}, {
    verbo: 'post',
    rota: '/api/pessoaEndereco/',
    metodo: controller.insert,
    public: true
}, {
    verbo: 'put',
    rota: '/api/pessoaEndereco/',
    metodo: controller.update,
    public: true
}, {
    verbo: 'delete',
    rota: '/api/pessoaEndereco/:id',
    metodo: controller.exclude,
    public: true
}
];