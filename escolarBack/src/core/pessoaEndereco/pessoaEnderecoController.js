const pessoaEnderecoRepository = require("./pessoaEnderecoRepository");
//   pessoaEnderecoService = require("./pessoaEnderecoService");

module.exports = {
  get,
  getById,
  insert,
  update,
  exclude
};

async function get(req, pg, recordCount) {
  let ret = await pessoaEnderecoRepository.get(pg);
  ret.recordCount = await pessoaEnderecoRepository.get({}, recordCount);  
  return ret;
}

async function getById(req) {
  return await pessoaEnderecoRepository.getById(req.params.id);
}

async function insert(req) {
  return await pessoaEnderecoRepository.insert(req.body.pessoaEndereco);
}

async function update(req) {
  return await pessoaEnderecoRepository.update(req.body.pessoaEndereco);
}

async function exclude(req) {
  return await pessoaEnderecoRepository.exclude(req.params.id);
}
