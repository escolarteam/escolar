const pessoaEndereco = require('./pessoaEnderecoModel')();

module.exports = {
    get,
    getById,
    insert,
    update,
    exclude
}

async function get(pg, recordCount) {
    return await pessoaEndereco.findAll({offset: pg.offset, limit: pg.limit, attributes: recordCount });
}

async function getById(id) {
    return await pessoaEndereco.findAll({ where: { ID: id } });
}

async function insert(pessoaEnderecoModel, transaction) {
    return await pessoaEndereco.create(pessoaEnderecoModel, { transaction });
}

async function update(pessoaEnderecoModel, transaction) {
    return await pessoaEndereco.update(pessoaEnderecoModel, { transaction, where: { ID: pessoaEnderecoModel.ID } });
}

async function exclude(id, transaction) {
    return await pessoaEndereco.destroy({ transaction, where: { ID: id } });
}

