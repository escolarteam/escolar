const pessoa = require('../pessoa/pessoaModel')(),
    endereco = require('../endereco/enderecoModel')(),
    sequelizeMYSQL = require('../../../config/sequelizeConfig'),
    sequelize = sequelizeMYSQL.sequelize(),
    DataTypes = sequelizeMYSQL.Sequelize.DataTypes;

module.exports = () => {
    return pessoaEndereco;
}

const pessoaEndereco = sequelize.define('pessoaEndereco', {
    ID: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    CREATED_BY: {
        type: DataTypes.STRING,
        allowNull: false
    },
    CREATED_ON: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW
    },
    ALTER_BY: {
        type: DataTypes.STRING,
        allowNull: false
    },
    ALTER_ON: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW
    },
    PESSOA: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    ENDERECO: {
        type: DataTypes.INTEGER,
        allowNull: false
    }
},
    {
        tableName: "pessoa_endereco",
        timestamps: false
    }
);

pessoaEndereco.belongsTo(pessoa, { foreignKey: 'PESSOA', targetKey: 'ID' });
pessoaEndereco.belongsTo(endereco, { foreignKey: 'ENDERECO', targetKey: 'ID' });
