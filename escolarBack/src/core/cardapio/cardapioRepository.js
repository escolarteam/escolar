const cardapio = require('./cardapioModel')();

module.exports = {
    get,
    getById,
    insert,
    update,
    exclude
}

async function get(pg, recordCount) {
    return await cardapio.findAll({offset: pg.offset, limit: pg.limit, attributes: recordCount });
}

async function getById(id) {
    return await cardapio.findAll({ where: { ID: id } });
}

async function insert(cardapioModel, transaction) {
    return await cardapio.create(cardapioModel, { transaction });
}

async function update(cardapioModel, transaction) {
    return await cardapio.update(cardapioModel, { transaction, where: { ID: cardapioModel.ID } });
}

async function exclude(id, transaction) {
    return await cardapio.destroy({ transaction, where: { ID: id } });
}

