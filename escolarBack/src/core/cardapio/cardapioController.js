const cardapioRepository = require("./cardapioRepository");
//   cardapioService = require("./cardapioService");

module.exports = {
  get,
  getById,
  insert,
  update,
  exclude
};

async function get(req, pg, recordCount) {
  let ret = await cardapioRepository.get(pg);
  ret.recordCount = await cardapioRepository.get({}, recordCount);  
  return ret;
}

async function getById(req) {
  return await cardapioRepository.getById(req.params.id);
}

async function insert(req) {
  return await cardapioRepository.insert(req.body.cardapio);
}

async function update(req) {
  return await cardapioRepository.update(req.body.cardapio);
}

async function exclude(req) {
  return await cardapioRepository.exclude(req.params.id);
}
