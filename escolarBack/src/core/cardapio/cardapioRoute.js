const controller = require('./cardapioController');

module.exports = [{
    verbo: 'get',
    rota: '/api/cardapio/',
    metodo: controller.get,
    public: true
}, {
    verbo: 'get',
    rota: '/api/cardapio/:id',
    metodo: controller.getById,
    public: true
}, {
    verbo: 'post',
    rota: '/api/cardapio/',
    metodo: controller.insert,
    public: true
}, {
    verbo: 'put',
    rota: '/api/cardapio/',
    metodo: controller.update,
    public: true
}, {
    verbo: 'delete',
    rota: '/api/cardapio/:id',
    metodo: controller.exclude,
    public: true
}
];