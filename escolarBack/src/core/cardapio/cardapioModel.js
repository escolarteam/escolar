const sequelizeMYSQL = require('../../../config/sequelizeConfig'),
    pessoa = require('../pessoa/pessoaModel')(),
    sequelize = sequelizeMYSQL.sequelize(),
    DataTypes = sequelizeMYSQL.Sequelize.DataTypes;

module.exports = () => {
    return cardapio;
}

const cardapio = sequelize.define('cardapio', {
    ID: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    CREATED_BY: {
        type: DataTypes.STRING,
        allowNull: false
    },
    CREATED_ON: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW
    },
    ALTER_BY: {
        type: DataTypes.STRING,
        allowNull: false
    },
    ALTER_ON: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW
    },
    DATA_FIM: {
        type: DataTypes.DATE,
        allowNull: false
    },
    PESSOA: {
        type: DataTypes.INTEGER,
        allowNull: false
    }
},
    {
        tableName: "cardapio",
        timestamps: false
    }
);

cardapio.belongsTo(pessoa, { foreignKey: 'PESSOA', targetKey: 'ID' });

