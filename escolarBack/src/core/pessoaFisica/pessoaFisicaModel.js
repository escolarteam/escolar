const pessoa = require('../pessoa/pessoaModel')(), 
    sequelizeMYSQL = require('../../../config/sequelizeConfig'),
    sequelize = sequelizeMYSQL.sequelize(),
    DataTypes = sequelizeMYSQL.Sequelize.DataTypes;

module.exports = () => {
    return pessoaFisica;
}

const pessoaFisica = sequelize.define('pessoaFisica', {
    ID: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    CREATED_BY: {
        type: DataTypes.STRING,
        allowNull: false
    },
    CREATED_ON: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW
    },
    ALTER_BY: {
        type: DataTypes.STRING,
        allowNull: false
    },
    ALTER_ON: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW
    },
    CPF: {
        type: DataTypes.STRING,
        allowNull: false
    },
    RG: {
        type: DataTypes.STRING,
        allowNull: false
    },
    PESSOA: {
        type: DataTypes.INTEGER,
        allowNull: false
    }
},
    {
        tableName: "pessoa_fisica",
        timestamps: false
    }
);

pessoaFisica.belongsTo(pessoa, { foreignKey: 'PESSOA', targetKey: 'ID' });
