const pessoaFisica = require('./pessoaFisicaModel')();

module.exports = {
    get,
    getById,
    insert,
    update,
    exclude
}

async function get(pg, recordCount) {
    return await pessoaFisica.findAll({offset: pg.offset, limit: pg.limit, attributes: recordCount });
}

async function getById(id) {
    return await pessoaFisica.findAll({ where: { ID: id } });
}

async function insert(pessoaFisicaModel, transaction) {
    return await pessoaFisica.create(pessoaFisicaModel, { transaction });
}

async function update(pessoaFisicaModel, transaction) {
    return await pessoaFisica.update(pessoaFisicaModel, { transaction, where: { ID: pessoaFisicaModel.ID } });
}

async function exclude(id, transaction) {
    return await pessoaFisica.destroy({ transaction, where: { ID: id } });
}

