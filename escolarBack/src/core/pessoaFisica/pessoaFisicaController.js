const pessoaFisicaRepository = require("./pessoaFisicaRepository");
//   pessoaFisicaService = require("./pessoaFisicaService");

module.exports = {
  get,
  getById,
  insert,
  update,
  exclude
};

async function get(req, pg, recordCount) {
  let ret = await pessoaFisicaRepository.get(pg);
  ret.recordCount = await pessoaFisicaRepository.get({}, recordCount);  
  return ret;
}

async function getById(req) {
  return await pessoaFisicaRepository.getById(req.params.id);
}

async function insert(req) {
  return await pessoaFisicaRepository.insert(req.body.pessoaFisica);
}

async function update(req) {
  return await pessoaFisicaRepository.update(req.body.pessoaFisica);
}

async function exclude(req) {
  return await pessoaFisicaRepository.exclude(req.params.id);
}
