const controller = require('./pessoaFisicaController');

module.exports = [{
    verbo: 'get',
    rota: '/api/pessoaFisica/',
    metodo: controller.get,
    public: true
}, {
    verbo: 'get',
    rota: '/api/pessoaFisica/:id',
    metodo: controller.getById,
    public: true
}, {
    verbo: 'post',
    rota: '/api/pessoaFisica/',
    metodo: controller.insert,
    public: true
}, {
    verbo: 'put',
    rota: '/api/pessoaFisica/',
    metodo: controller.update,
    public: true
}, {
    verbo: 'delete',
    rota: '/api/pessoaFisica/:id',
    metodo: controller.exclude,
    public: true
}
];