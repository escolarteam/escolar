const perfilRepository = require("./perfilRepository");
//   perfilService = require("./perfilService");

module.exports = {
  get,
  getById,
  insert,
  update,
  exclude
};

async function get(req, pg, recordCount) {
  let ret = await perfilRepository.get(pg);
  ret.recordCount = await perfilRepository.get({}, recordCount);  
  return ret;
}

async function getById(req) {
  return await perfilRepository.getById(req.params.id);
}

async function insert(req) {
  return await perfilRepository.insert(req.body.perfil);
}

async function update(req) {
  return await perfilRepository.update(req.body.perfil);
}

async function exclude(req) {
  return await perfilRepository.exclude(req.params.id);
}
