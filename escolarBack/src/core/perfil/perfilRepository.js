const perfil = require('./perfilModel')();

module.exports = {
    get,
    getById,
    insert,
    update,
    exclude
}

async function get(pg, recordCount) {
    return await perfil.findAll({offset: pg.offset, limit: pg.limit, attributes: recordCount });
}

async function getById(id) {
    return await perfil.findAll({ where: { ID: id } });
}

async function insert(perfilModel, transaction) {
    return await perfil.create(perfilModel, { transaction });
}

async function update(perfilModel, transaction) {
    return await perfil.update(perfilModel, { transaction, where: { ID: perfilModel.ID } });
}

async function exclude(id, transaction) {
    return await perfil.destroy({ transaction, where: { ID: id } });
}

