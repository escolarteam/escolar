const controller = require('./perfilController');

module.exports = [{
    verbo: 'get',
    rota: '/api/perfil/',
    metodo: controller.get,
    public: true
}, {
    verbo: 'get',
    rota: '/api/perfil/:id',
    metodo: controller.getById,
    public: true
}, {
    verbo: 'post',
    rota: '/api/perfil/',
    metodo: controller.insert,
    public: true
}, {
    verbo: 'put',
    rota: '/api/perfil/',
    metodo: controller.update,
    public: true
}, {
    verbo: 'delete',
    rota: '/api/perfil/:id',
    metodo: controller.exclude,
    public: true
}
];