const matricula = require('./matriculaModel')();

module.exports = {
    get,
    getById,
    insert,
    update,
    exclude
}

async function get(pg, recordCount) {
    return await matricula.findAll({offset: pg.offset, limit: pg.limit, attributes: recordCount });
}

async function getById(id) {
    return await matricula.findAll({ where: { ID: id } });
}

async function insert(matriculaModel, transaction) {
    return await matricula.create(matriculaModel, { transaction });
}

async function update(matriculaModel, transaction) {
    return await matricula.update(matriculaModel, { transaction, where: { ID: matriculaModel.ID } });
}

async function exclude(id, transaction) {
    return await matricula.destroy({ transaction, where: { ID: id } });
}

