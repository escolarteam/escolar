const controller = require('./matriculaController');

module.exports = [{
    verbo: 'get',
    rota: '/api/matricula/',
    metodo: controller.get,
    public: true
}, {
    verbo: 'get',
    rota: '/api/matricula/:id',
    metodo: controller.getById,
    public: true
}, {
    verbo: 'post',
    rota: '/api/matricula/',
    metodo: controller.insert,
    public: true
}, {
    verbo: 'put',
    rota: '/api/matricula/',
    metodo: controller.update,
    public: true
}, {
    verbo: 'delete',
    rota: '/api/matricula/:id',
    metodo: controller.exclude,
    public: true
}
];