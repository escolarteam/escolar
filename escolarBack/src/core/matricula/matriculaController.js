const matriculaRepository = require("./matriculaRepository");
//   matriculaService = require("./matriculaService");

module.exports = {
  get,
  getById,
  insert,
  update,
  exclude
};

async function get(req, pg, recordCount) {
  let ret = await matriculaRepository.get(pg);
  ret.recordCount = await matriculaRepository.get({}, recordCount);  
  return ret;
}

async function getById(req) {
  return await matriculaRepository.getById(req.params.id);
}

async function insert(req) {
  return await matriculaRepository.insert(req.body.matricula);
}

async function update(req) {
  return await matriculaRepository.update(req.body.matricula);
}

async function exclude(req) {
  return await matriculaRepository.exclude(req.params.id);
}
