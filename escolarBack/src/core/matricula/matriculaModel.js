const turma = require('../turma/turmaModel')(),
    pessoa = require('../pessoa/pessoaModel')(),
    sequelizeMYSQL = require('../../../config/sequelizeConfig'),
    sequelize = sequelizeMYSQL.sequelize(),
    DataTypes = sequelizeMYSQL.Sequelize.DataTypes;

module.exports = () => {
    return matricula;
}

const matricula = sequelize.define('matricula', {
    ID: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    CREATED_BY: {
        type: DataTypes.STRING,
        allowNull: false
    },
    CREATED_ON: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW
    },
    ALTER_BY: {
        type: DataTypes.STRING,
        allowNull: false
    },
    ALTER_ON: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW
    },
    DATA_FIM: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW
    },
    ALUNO: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    TURMA: {
        type: DataTypes.INTEGER,
        allowNull: false
    }
},
    {
        tableName: "matricula",
        timestamps: false
    }
);

matricula.belongsTo(turma, { foreignKey: 'TURMA', targetKey: 'ID' });
matricula.belongsTo(pessoa, { foreignKey: 'ALUNO', targetKey: 'ID' });
