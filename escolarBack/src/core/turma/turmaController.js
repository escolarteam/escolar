const turmaRepository = require("./turmaRepository");
//   turmaService = require("./turmaService");

module.exports = {
  get,
  getById,
  insert,
  update,
  exclude
};

async function get(req, pg, recordCount) {
  let ret = await turmaRepository.get(pg);
  ret.recordCount = await turmaRepository.get({}, recordCount);  
  return ret;
}

async function getById(req) {
  return await turmaRepository.getById(req.params.id);
}

async function insert(req) {
  return await turmaRepository.insert(req.body.turma);
}

async function update(req) {
  return await turmaRepository.update(req.body.turma);
}

async function exclude(req) {
  return await turmaRepository.exclude(req.params.id);
}
