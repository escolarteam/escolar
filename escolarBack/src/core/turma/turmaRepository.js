const turma = require('./turmaModel')();

module.exports = {
    get,
    getById,
    insert,
    update,
    exclude
}

async function get(pg, recordCount) {
    return await turma.findAll({offset: pg.offset, limit: pg.limit, attributes: recordCount });
}

async function getById(id) {
    return await turma.findAll({ where: { ID: id } });
}

async function insert(turmaModel, transaction) {
    return await turma.create(turmaModel, { transaction });
}

async function update(turmaModel, transaction) {
    return await turma.update(turmaModel, { transaction, where: { ID: turmaModel.ID } });
}

async function exclude(id, transaction) {
    return await turma.destroy({ transaction, where: { ID: id } });
}

