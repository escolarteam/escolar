const controller = require('./turmaController');

module.exports = [{
    verbo: 'get',
    rota: '/api/turma/',
    metodo: controller.get,
    public: true
}, {
    verbo: 'get',
    rota: '/api/turma/:id',
    metodo: controller.getById,
    public: true
}, {
    verbo: 'post',
    rota: '/api/turma/',
    metodo: controller.insert,
    public: true
}, {
    verbo: 'put',
    rota: '/api/turma/',
    metodo: controller.update,
    public: true
}, {
    verbo: 'delete',
    rota: '/api/turma/:id',
    metodo: controller.exclude,
    public: true
}
];