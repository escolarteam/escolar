const controller = require('./telefoneController');

module.exports = [{
    verbo: 'get',
    rota: '/api/telefone/',
    metodo: controller.get,
    public: true
}, {
    verbo: 'get',
    rota: '/api/telefone/:id',
    metodo: controller.getById,
    public: true
}, {
    verbo: 'post',
    rota: '/api/telefone/',
    metodo: controller.insert,
    public: true
}, {
    verbo: 'put',
    rota: '/api/telefone/',
    metodo: controller.update,
    public: true
}, {
    verbo: 'delete',
    rota: '/api/telefone/:id',
    metodo: controller.exclude,
    public: true
}
];