const telefoneRepository = require("./telefoneRepository");
//   telefoneService = require("./telefoneService");

module.exports = {
  get,
  getById,
  insert,
  update,
  exclude
};

async function get(req, pg, recordCount) {
  let ret = await telefoneRepository.get(pg);
  ret.recordCount = await telefoneRepository.get({}, recordCount);  
  return ret;
}

async function getById(req) {
  return await telefoneRepository.getById(req.params.id);
}

async function insert(req) {
  return await telefoneRepository.insert(req.body.telefone);
}

async function update(req) {
  return await telefoneRepository.update(req.body.telefone);
}

async function exclude(req) {
  return await telefoneRepository.exclude(req.params.id);
}
