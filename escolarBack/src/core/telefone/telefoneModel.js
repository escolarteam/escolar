const tipoTelefone = require('../tipoTelefone/tipoTelefoneModel')(), 
    sequelizeMYSQL = require('../../../config/sequelizeConfig'),
    sequelize = sequelizeMYSQL.sequelize(),
    DataTypes = sequelizeMYSQL.Sequelize.DataTypes;

module.exports = () => {
    return telefone;
}

const telefone = sequelize.define('telefone', {
    ID: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    CREATED_BY: {
        type: DataTypes.STRING,
        allowNull: false
    },
    CREATED_ON: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW
    },
    ALTER_BY: {
        type: DataTypes.STRING,
        allowNull: false
    },
    ALTER_ON: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW
    },
    NUMERO: {
        type: DataTypes.STRING,
        allowNull: false
    },
    TIPO_TELEFONE: {
        type: DataTypes.INTEGER,
        allowNull: false
    }
},
    {
        tableName: "telefone",
        timestamps: false
    }
);

telefone.belongsTo(tipoTelefone, { foreignKey: 'TIPO_TELEFONE', targetKey: 'ID' });
