const telefone = require('./telefoneModel')();

module.exports = {
    get,
    getById,
    insert,
    update,
    exclude
}

async function get(pg, recordCount) {
    return await telefone.findAll({offset: pg.offset, limit: pg.limit, attributes: recordCount });
}

async function getById(id) {
    return await telefone.findAll({ where: { ID: id } });
}

async function insert(telefoneModel, transaction) {
    return await telefone.create(telefoneModel, { transaction });
}

async function update(telefoneModel, transaction) {
    return await telefone.update(telefoneModel, { transaction, where: { ID: telefoneModel.ID } });
}

async function exclude(id, transaction) {
    return await telefone.destroy({ transaction, where: { ID: id } });
}

