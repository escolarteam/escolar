const refeicaoRepository = require("./refeicaoRepository");
//   refeicaoService = require("./refeicaoService");

module.exports = {
  get,
  getById,
  insert,
  update,
  exclude
};

async function get(req, pg, recordCount) {
  let ret = await refeicaoRepository.get(pg);
  ret.recordCount = await refeicaoRepository.get({}, recordCount);  
  return ret;
}

async function getById(req) {
  return await refeicaoRepository.getById(req.params.id);
}

async function insert(req) {
  return await refeicaoRepository.insert(req.body.refeicao);
}

async function update(req) {
  return await refeicaoRepository.update(req.body.refeicao);
}

async function exclude(req) {
  return await refeicaoRepository.exclude(req.params.id);
}
