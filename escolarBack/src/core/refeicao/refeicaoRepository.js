const refeicao = require('./refeicaoModel')();

module.exports = {
    get,
    getById,
    insert,
    update,
    exclude
}

async function get(pg, recordCount) {
    return await refeicao.findAll({offset: pg.offset, limit: pg.limit, attributes: recordCount });
}

async function getById(id) {
    return await refeicao.findAll({ where: { ID: id } });
}

async function insert(refeicaoModel, transaction) {
    return await refeicao.create(refeicaoModel, { transaction });
}

async function update(refeicaoModel, transaction) {
    return await refeicao.update(refeicaoModel, { transaction, where: { ID: refeicaoModel.ID } });
}

async function exclude(id, transaction) {
    return await refeicao.destroy({ transaction, where: { ID: id } });
}

