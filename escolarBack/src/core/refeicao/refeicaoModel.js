const tipoRefeicao = require('../tipoRefeicao/tipoRefeicaoModel')(), 
    sequelizeMYSQL = require('../../../config/sequelizeConfig'),
    sequelize = sequelizeMYSQL.sequelize(),
    DataTypes = sequelizeMYSQL.Sequelize.DataTypes;

module.exports = () => {
    return refeicao;
}

const refeicao = sequelize.define('refeicao', {
    ID: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    CREATED_BY: {
        type: DataTypes.STRING,
        allowNull: false
    },
    CREATED_ON: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW
    },
    ALTER_BY: {
        type: DataTypes.STRING,
        allowNull: false
    },
    ALTER_ON: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW
    },
    HORARIO: {
        type: DataTypes.DATE,
        allowNull: false
    },
    TIPO_REFEICAO: {
        type: DataTypes.INTEGER,
        allowNull: false
    }
},
    {
        tableName: "refeicao",
        timestamps: false
    }
);

refeicao.belongsTo(tipoRefeicao, { foreignKey: 'TIPO_REFEICAO', targetKey: 'ID' });
