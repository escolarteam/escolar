const controller = require('./refeicaoController');

module.exports = [{
    verbo: 'get',
    rota: '/api/refeicao/',
    metodo: controller.get,
    public: true
}, {
    verbo: 'get',
    rota: '/api/refeicao/:id',
    metodo: controller.getById,
    public: true
}, {
    verbo: 'post',
    rota: '/api/refeicao/',
    metodo: controller.insert,
    public: true
}, {
    verbo: 'put',
    rota: '/api/refeicao/',
    metodo: controller.update,
    public: true
}, {
    verbo: 'delete',
    rota: '/api/refeicao/:id',
    metodo: controller.exclude,
    public: true
}
];