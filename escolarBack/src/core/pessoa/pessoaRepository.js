const pessoa = require('./pessoaModel')();

module.exports = {
    get,
    getById,
    insert,
    update,
    exclude
}

async function get(pg, recordCount) {
    return await pessoa.findAll({offset: pg.offset, limit: pg.limit, attributes: recordCount });
}

async function getById(id) {
    return await pessoa.findAll({ where: { ID: id } });
}

async function insert(pessoaModel, transaction) {
    return await pessoa.create(pessoaModel, { transaction });
}

async function update(pessoaModel, transaction) {
    return await pessoa.update(pessoaModel, { transaction, where: { ID: pessoaModel.ID } });
}

async function exclude(id, transaction) {
    return await pessoa.destroy({ transaction, where: { ID: id } });
}

