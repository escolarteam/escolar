const controller = require('./pessoaController');

module.exports = [{
    verbo: 'get',
    rota: '/api/pessoa/',
    metodo: controller.get,
    public: true
}, {
    verbo: 'get',
    rota: '/api/pessoa/:id',
    metodo: controller.getById,
    public: true
}, {
    verbo: 'post',
    rota: '/api/pessoa/',
    metodo: controller.insert,
    public: true
}, {
    verbo: 'put',
    rota: '/api/pessoa/',
    metodo: controller.update,
    public: true
}, {
    verbo: 'delete',
    rota: '/api/pessoa/:id',
    metodo: controller.exclude,
    public: true
}
];