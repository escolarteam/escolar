const pessoaRepository = require("./pessoaRepository");
//   pessoaService = require("./pessoaService");

module.exports = {
  get,
  getById,
  insert,
  update,
  exclude
};

async function get(req, pg, recordCount) {
  let ret = await pessoaRepository.get(pg);
  ret.recordCount = await pessoaRepository.get({}, recordCount);  
  return ret;
}

async function getById(req) {
  return await pessoaRepository.getById(req.params.id);
}

async function insert(req) {
  return await pessoaRepository.insert(req.body.pessoa);
}

async function update(req) {
  return await pessoaRepository.update(req.body.pessoa);
}

async function exclude(req) {
  return await pessoaRepository.exclude(req.params.id);
}
