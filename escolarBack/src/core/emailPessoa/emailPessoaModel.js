const pessoa = require('../pessoa/pessoaModel')(),
    tipoEmail = require('../tipoEmail/tipoEmailModel')(), 
    sequelizeMYSQL = require('../../../config/sequelizeConfig'),
    sequelize = sequelizeMYSQL.sequelize(),
    DataTypes = sequelizeMYSQL.Sequelize.DataTypes;

module.exports = () => {
    return emailPessoa;
}

const emailPessoa = sequelize.define('emailPessoa', {
    ID: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    CREATED_BY: {
        type: DataTypes.STRING,
        allowNull: false
    },
    CREATED_ON: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW
    },
    ALTER_BY: {
        type: DataTypes.STRING,
        allowNull: false
    },
    ALTER_ON: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW
    },
    EMAIL: {
        type: DataTypes.STRING,
        allowNull: false
    },
    TIPO_EMAIL: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    PESSOA: {
        type: DataTypes.INTEGER,
        allowNull: false
    }
},
    {
        tableName: "email_pessoa",
        timestamps: false
    }
);

emailPessoa.belongsTo(pessoa, { foreignKey: 'PESSOA', targetKey: 'ID' });
emailPessoa.belongsTo(tipoEmail, { foreignKey: 'TIPO_EMAIL', targetKey: 'ID' });
