const controller = require('./emailPessoaController');

module.exports = [{
    verbo: 'get',
    rota: '/api/emailPessoa/',
    metodo: controller.get,
    public: true
}, {
    verbo: 'get',
    rota: '/api/emailPessoa/:id',
    metodo: controller.getById,
    public: true
}, {
    verbo: 'post',
    rota: '/api/emailPessoa/',
    metodo: controller.insert,
    public: true
}, {
    verbo: 'put',
    rota: '/api/emailPessoa/',
    metodo: controller.update,
    public: true
}, {
    verbo: 'delete',
    rota: '/api/emailPessoa/:id',
    metodo: controller.exclude,
    public: true
}
];