const emailPessoaRepository = require("./emailPessoaRepository");
//   emailPessoaService = require("./emailPessoaService");

module.exports = {
  get,
  getById,
  insert,
  update,
  exclude
};

async function get(req, pg, recordCount) {
  let ret = await emailPessoaRepository.get(pg);
  ret.recordCount = await emailPessoaRepository.get({}, recordCount);  
  return ret;
}

async function getById(req) {
  return await emailPessoaRepository.getById(req.params.id);
}

async function insert(req) {
  return await emailPessoaRepository.insert(req.body.emailPessoa);
}

async function update(req) {
  return await emailPessoaRepository.update(req.body.emailPessoa);
}

async function exclude(req) {
  return await emailPessoaRepository.exclude(req.params.id);
}
