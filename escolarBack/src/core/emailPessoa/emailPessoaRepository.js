const emailPessoa = require('./emailPessoaModel')();

module.exports = {
    get,
    getById,
    insert,
    update,
    exclude
}

async function get(pg, recordCount) {
    return await emailPessoa.findAll({offset: pg.offset, limit: pg.limit, attributes: recordCount });
}

async function getById(id) {
    return await emailPessoa.findAll({ where: { ID: id } });
}

async function insert(emailPessoaModel, transaction) {
    return await emailPessoa.create(emailPessoaModel, { transaction });
}

async function update(emailPessoaModel, transaction) {
    return await emailPessoa.update(emailPessoaModel, { transaction, where: { ID: emailPessoaModel.ID } });
}

async function exclude(id, transaction) {
    return await emailPessoa.destroy({ transaction, where: { ID: id } });
}

