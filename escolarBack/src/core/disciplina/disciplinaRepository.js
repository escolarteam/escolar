const disciplina = require('./disciplinaModel')();

module.exports = {
    get,
    getById,
    insert,
    update,
    exclude
}

async function get(pg, recordCount) {
    return await disciplina.findAll({offset: pg.offset, limit: pg.limit, attributes: recordCount });
}

async function getById(id) {
    return await disciplina.findAll({ where: { ID: id } });
}

async function insert(disciplinaModel, transaction) {
    return await disciplina.create(disciplinaModel, { transaction });
}

async function update(disciplinaModel, transaction) {
    return await disciplina.update(disciplinaModel, { transaction, where: { ID: disciplinaModel.ID } });
}

async function exclude(id, transaction) {
    return await disciplina.destroy({ transaction, where: { ID: id } });
}

