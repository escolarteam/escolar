const controller = require('./disciplinaController');

module.exports = [{
    verbo: 'get',
    rota: '/api/disciplina/',
    metodo: controller.get,
    public: true
}, {
    verbo: 'get',
    rota: '/api/disciplina/:id',
    metodo: controller.getById,
    public: true
}, {
    verbo: 'post',
    rota: '/api/disciplina/',
    metodo: controller.insert,
    public: true
}, {
    verbo: 'put',
    rota: '/api/disciplina/',
    metodo: controller.update,
    public: true
}, {
    verbo: 'delete',
    rota: '/api/disciplina/:id',
    metodo: controller.exclude,
    public: true
}
];