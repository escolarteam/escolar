const turma = require('../turma/turmaModel')(), 
    sequelizeMYSQL = require('../../../config/sequelizeConfig'),
    sequelize = sequelizeMYSQL.sequelize(),
    DataTypes = sequelizeMYSQL.Sequelize.DataTypes;

module.exports = () => {
    return disciplina;
}

const disciplina = sequelize.define('disciplina', {
    ID: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    CREATED_BY: {
        type: DataTypes.STRING,
        allowNull: false
    },
    CREATED_ON: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW
    },
    ALTER_BY: {
        type: DataTypes.STRING,
        allowNull: false
    },
    ALTER_ON: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW
    },
    DESCRICAO: {
        type: DataTypes.STRING,
        allowNull: false
    },
    TURMA: {
        type: DataTypes.INTEGER,
        allowNull: false
    }
},
    {
        tableName: "disciplina",
        timestamps: false
    }
);

disciplina.belongsTo(turma, { foreignKey: 'TURMA', targetKey: 'ID' });
