const disciplinaRepository = require("./disciplinaRepository");
//   disciplinaService = require("./disciplinaService");

module.exports = {
  get,
  getById,
  insert,
  update,
  exclude
};

async function get(req, pg, recordCount) {
  let ret = await disciplinaRepository.get(pg);
  ret.recordCount = await disciplinaRepository.get({}, recordCount);  
  return ret;
}

async function getById(req) {
  return await disciplinaRepository.getById(req.params.id);
}

async function insert(req) {
  return await disciplinaRepository.insert(req.body.disciplina);
}

async function update(req) {
  return await disciplinaRepository.update(req.body.disciplina);
}

async function exclude(req) {
  return await disciplinaRepository.exclude(req.params.id);
}
