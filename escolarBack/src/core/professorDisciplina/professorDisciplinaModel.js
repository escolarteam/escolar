const pessoa = require('../pessoa/pessoaModel')(),
    disciplina = require('../disciplina/disciplinaModel')(),
    planoAula = require('../planoAula/planoAulaModel')(),
    sequelizeMYSQL = require('../../../config/sequelizeConfig'),
    sequelize = sequelizeMYSQL.sequelize(),
    DataTypes = sequelizeMYSQL.Sequelize.DataTypes;

module.exports = () => {
    return professorDisciplina;
}

const professorDisciplina = sequelize.define('professorDisciplina', {
    ID: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    CREATED_BY: {
        type: DataTypes.STRING,
        allowNull: false
    },
    CREATED_ON: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW
    },
    ALTER_BY: {
        type: DataTypes.STRING,
        allowNull: false
    },
    ALTER_ON: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW
    },
    PESSOA: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    DISCIPLINA: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    PLANO_AULA: {
        type: DataTypes.INTEGER,
        allowNull: false
    }
},
    {
        tableName: "professor_disciplina",
        timestamps: false
    }
);

professorDisciplina.belongsTo(pessoa, { foreignKey: 'PESSOA', targetKey: 'ID' });
professorDisciplina.belongsTo(disciplina, { foreignKey: 'DISCIPLINA', targetKey: 'ID' });
professorDisciplina.belongsTo(planoAula, { foreignKey: 'PLANO_AULA', targetKey: 'ID' });
