const professorDisciplinaRepository = require("./professorDisciplinaRepository");
//   professorDisciplinaService = require("./professorDisciplinaService");

module.exports = {
  get,
  getById,
  insert,
  update,
  exclude
};

async function get(req, pg, recordCount) {
  let ret = await professorDisciplinaRepository.get(pg);
  ret.recordCount = await professorDisciplinaRepository.get({}, recordCount);  
  return ret;
}

async function getById(req) {
  return await professorDisciplinaRepository.getById(req.params.id);
}

async function insert(req) {
  return await professorDisciplinaRepository.insert(req.body.professorDisciplina);
}

async function update(req) {
  return await professorDisciplinaRepository.update(req.body.professorDisciplina);
}

async function exclude(req) {
  return await professorDisciplinaRepository.exclude(req.params.id);
}
