const professorDisciplina = require('./professorDisciplinaModel')();

module.exports = {
    get,
    getById,
    insert,
    update,
    exclude
}

async function get(pg, recordCount) {
    return await professorDisciplina.findAll({offset: pg.offset, limit: pg.limit, attributes: recordCount });
}

async function getById(id) {
    return await professorDisciplina.findAll({ where: { ID: id } });
}

async function insert(professorDisciplinaModel, transaction) {
    return await professorDisciplina.create(professorDisciplinaModel, { transaction });
}

async function update(professorDisciplinaModel, transaction) {
    return await professorDisciplina.update(professorDisciplinaModel, { transaction, where: { ID: professorDisciplinaModel.ID } });
}

async function exclude(id, transaction) {
    return await professorDisciplina.destroy({ transaction, where: { ID: id } });
}

