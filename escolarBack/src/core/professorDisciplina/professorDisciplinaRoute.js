const controller = require('./professorDisciplinaController');

module.exports = [{
    verbo: 'get',
    rota: '/api/professorDisciplina/',
    metodo: controller.get,
    public: true
}, {
    verbo: 'get',
    rota: '/api/professorDisciplina/:id',
    metodo: controller.getById,
    public: true
}, {
    verbo: 'post',
    rota: '/api/professorDisciplina/',
    metodo: controller.insert,
    public: true
}, {
    verbo: 'put',
    rota: '/api/professorDisciplina/',
    metodo: controller.update,
    public: true
}, {
    verbo: 'delete',
    rota: '/api/professorDisciplina/:id',
    metodo: controller.exclude,
    public: true
}
];