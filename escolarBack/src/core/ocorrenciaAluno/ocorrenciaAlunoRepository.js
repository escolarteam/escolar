const ocorrenciaAluno = require('./ocorrenciaAlunoModel')();

module.exports = {
    get,
    getById,
    insert,
    update,
    exclude
}

async function get(pg, recordCount) {
    return await ocorrenciaAluno.findAll({offset: pg.offset, limit: pg.limit, attributes: recordCount });
}

async function getById(id) {
    return await ocorrenciaAluno.findAll({ where: { ID: id } });
}

async function insert(ocorrenciaAlunoModel, transaction) {
    return await ocorrenciaAluno.create(ocorrenciaAlunoModel, { transaction });
}

async function update(ocorrenciaAlunoModel, transaction) {
    return await ocorrenciaAluno.update(ocorrenciaAlunoModel, { transaction, where: { ID: ocorrenciaAlunoModel.ID } });
}

async function exclude(id, transaction) {
    return await ocorrenciaAluno.destroy({ transaction, where: { ID: id } });
}

