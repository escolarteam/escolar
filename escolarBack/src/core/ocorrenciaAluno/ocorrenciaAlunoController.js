const ocorrenciaAlunoRepository = require("./ocorrenciaAlunoRepository");
//   ocorrenciaAlunoService = require("./ocorrenciaAlunoService");

module.exports = {
  get,
  getById,
  insert,
  update,
  exclude
};

async function get(req, pg, recordCount) {
  let ret = await ocorrenciaAlunoRepository.get(pg);
  ret.recordCount = await ocorrenciaAlunoRepository.get({}, recordCount);  
  return ret;
}

async function getById(req) {
  return await ocorrenciaAlunoRepository.getById(req.params.id);
}

async function insert(req) {
  return await ocorrenciaAlunoRepository.insert(req.body.ocorrenciaAluno);
}

async function update(req) {
  return await ocorrenciaAlunoRepository.update(req.body.ocorrenciaAluno);
}

async function exclude(req) {
  return await ocorrenciaAlunoRepository.exclude(req.params.id);
}
