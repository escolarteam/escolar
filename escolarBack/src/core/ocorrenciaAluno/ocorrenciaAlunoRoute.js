const controller = require('./ocorrenciaAlunoController');

module.exports = [{
    verbo: 'get',
    rota: '/api/ocorrenciaAluno/',
    metodo: controller.get,
    public: true
}, {
    verbo: 'get',
    rota: '/api/ocorrenciaAluno/:id',
    metodo: controller.getById,
    public: true
}, {
    verbo: 'post',
    rota: '/api/ocorrenciaAluno/',
    metodo: controller.insert,
    public: true
}, {
    verbo: 'put',
    rota: '/api/ocorrenciaAluno/',
    metodo: controller.update,
    public: true
}, {
    verbo: 'delete',
    rota: '/api/ocorrenciaAluno/:id',
    metodo: controller.exclude,
    public: true
}
];