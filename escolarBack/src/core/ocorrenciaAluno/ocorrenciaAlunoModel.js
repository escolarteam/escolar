const pessoa = require('../pessoa/pessoaModel')(), 
    ocorrencia = require('../ocorrencia/ocorrenciaModel')(),
    sequelizeMYSQL = require('../../../config/sequelizeConfig'),
    sequelize = sequelizeMYSQL.sequelize(),
    DataTypes = sequelizeMYSQL.Sequelize.DataTypes;

module.exports = () => {
    return ocorrenciaAluno;
}

const ocorrenciaAluno = sequelize.define('ocorrenciaAluno', {
    ID: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    CREATED_BY: {
        type: DataTypes.STRING,
        allowNull: false
    },
    CREATED_ON: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW
    },
    ALTER_BY: {
        type: DataTypes.STRING,
        allowNull: false
    },
    ALTER_ON: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW
    },
    OCORRENCIA: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    PESSOA: {
        type: DataTypes.INTEGER,
        allowNull: false
    }
},
    {
        tableName: "ocorrencia_aluno",
        timestamps: false
    }
);

ocorrenciaAluno.belongsTo(ocorrencia, { foreignKey: 'OCORRENCIA', targetKey: 'ID' });
ocorrenciaAluno.belongsTo(pessoa, { foreignKey: 'PESSOA', targetKey: 'ID' });
