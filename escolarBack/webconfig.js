module.exports = {
    apis: {
        poseidon: {
            url: 'http://poseidoncore.luizalabs.com/',
            token: "fdd9fe38b70d934d9aafeb384e0698a962b8288c2867583bbbfc172290439710",
        },
        oneSignal: {
            url: 'https://onesignal.com/api/v1/',
            token: "Basic OWUzNzg0YWUtNGVmMC00NWE5LTgzNjYtNjBkZTNhMzU4ZDMy",
            appId: 'd3c53632-f84e-4654-90c0-1366f76e5d47'
        },
        apigee: getApigee(),
        workflow: getApiWorkflow(),
        saidaCaixa: getSaidaCaixa()
    },
    slack: {
        urlHook: process.env.urlHook || "https://hooks.slack.com/services/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",
        channel: process.env.channel || "status-luiza-travel",
        iconUrl: process.env.iconUrl,
        botusername: process.env.botusername
    },
    emailDebug: 'mateus@smnti.com.br',
    apiRequestTimeout: 20000,
    secretWord: process.env.NODE_ENV != "production" ? 'beterraba' : 'beterraba',//'goiaba',
    nameApi: 'Luiza Viagens Core',
    urlApi: 'http://localhost',
    portApi: process.env.PORT || 4510,
    dataConfig: {
        MYSQL: getBanco()
    },
    testConfig: {
        request: require('superagent'),
        divisor: '------------------------------------------------------------\n',
        usuarioLogado: "eyJ1c2VyIjp7InByaW1laXJvQWNlc3NvIjpmYWxzZSwiY29udGFjdCI6e30sImxvZ2luIjoiQURNX1ZJQUdFTlMiLCJwYXNzd29yZCI6IkFETTEyMyIsIklTX0ZPUk5FQ0VET1IiOjAsIkhBU19DT05UQSI6MCwiQkFHIjp7ImlzUm9vdCI6MSwiQkFHRVZFTlRPUyI6MSwiQkFHUE9MSVRJQ0FTIjoxLCJCQUdQQVJBTUVUUk9TIjoxLCJCQUdHUlVQT1MiOjEsImlzQ3NjTWVtYmVyIjoxLCJpc0Fwcm92YWRvciI6MSwiaXNBcHJvdmFkb3JFdmVudG8iOjEsImlzQ2FkYXN0cmFkb3IiOjEsImNhblNlYXJjaCI6MH19LCJ0b2tlbiI6IjkyMzM5NzMwMWQxNDQzMWNjMjk2ODU3MjAyYzExNDNmIn0="
    }
};

function getApigee() {
    if (process.env.NODE_ENV == "production") {
        console.log("[APIGEE] Conectado a PRODUÇÃO");
        return {
            url: 'https://api.apiluiza.com.br/v1/',
            token: "Bearer 3cUr3l9lCK2wz4auAbVhNRrM3JUK",
            systemCode: '01415'
        };
    }

    if (process.env.NODE_ENV == "staging") {
        console.log("[APIGEE] Conectado a STAGE");
        return {
            url: 'https://stage.apiluiza.com.br/v1/',
            token: "Bearer 9JtkZ4PtXAqcB5OAJ9nqbOawZljb",
            systemCode: '03828'
        };
    }

    console.log("[APIGEE] Conectado a TEST");
    return {
        url: 'https://test.apiluiza.com.br/v1/',
        token: "Bearer YOPZkqupKhb9TqfG0hPlZM1XRU6W",
        systemCode: '03828'
    };
}

function getSaidaCaixa() {
    if (process.env.NODE_ENV != "production") {
        return {
            url: 'https://stage.apiluiza.com.br/v1/',
            token: "Bearer qd0mxplCpZ8jv5KR1kVKJnKCHUsh",
            systemCode: '03828',
        }
    }
    return {
        url: 'https://api.apiluiza.com.br/v1/',
        token: "Bearer 3cUr3l9lCK2wz4auAbVhNRrM3JUK",
        systemCode: '01415'
    }
}

function getApiWorkflow() {
    if (process.env.NODE_ENV == "production") {
        return {
            url: 'http://a2affd60452b611e7ba4f0ab184f83c7-1476600299.sa-east-1.elb.amazonaws.com/',
            token: "EIFJOESJFOSJFOJSEFOHSIFHISDHFISHD"
        };
    }

    if (process.env.NODE_ENV == "staging") {
        return {
            url: 'http://a89241d2e785811e7a05d0e3fd73a446-1836552044.us-east-1.elb.amazonaws.com/',
            token: "EIFJOESJFOSJFOJSEFOHSIFHISDHFISHD"
        };
    }

    return {
        url: 'http://localhost:4503/',
        token: "EIFJOESJFOSJFOJSEFOHSIFHISDHFISHD"
    };
}

function getBanco() {
    if (process.env.NODE_ENV == "production") {
        console.log(`[MYSQL] Conectado a amazonaws port [${3306}]`);
        return {
            host: 'luizatraveldb.c8ze3i6lyjqu.sa-east-1.rds.amazonaws.com',
            user: 'luizatravel',
            database: 'luizatraveldb',
            password: 'ntev6ngSR9sS3nS8E35wE&RPivtn&k8D',
            port: 3306,
            dialect: 'mysql'
        }
    }

    if (process.env.NODE_ENV == "staging") {
        console.log(`[MYSQL] Conectado a Stage port [${3306}]`);
        return {
            host: 'kubernetes-apps-staging.cd2vfjltihkr.us-east-1.rds.amazonaws.com',
            user: 'luizatravel',
            database: 'luizatravel',
            password: 'Y3!tGUlG4Et@TR**Mpu@XwmbfpvceP6S',
            port: 3306,
            dialect: 'mysql'
        }
    }

    console.log(`[MYSQL] Conectado a localhost port [${3306}]`);
    return {
        host: 'localhost',
        user: 'root',
        database: 'escolar_db',
        password: 'root',
        port: 3306,
        dialect: 'mysql'
    }
}