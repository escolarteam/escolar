const fs = require("fs");

describe("Inicio dos testes: ", () => {
    // fs.readdir("src/core/", (err, files) => {
    //     files.forEach(element => {
    //         describe(`1 - Iniciando teste funcionalidade: ${element}`, await require(`../src/core/${element}/${element}Test`))
    //     });
    // });
    describe(`1 - Iniciando teste funcionalidade: usuarioAprovador`, require(`../src/core/usuarioAprovador/usuarioAprovadorTest`));
    describe(`1 - Iniciando teste funcionalidade: workflow`, require(`../src/core/workflow/workflowTest`));
});
